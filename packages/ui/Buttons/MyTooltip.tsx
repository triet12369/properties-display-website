import { Tooltip, TooltipProps, useMantineTheme } from '@mantine/core';
import { FC, forwardRef, PropsWithChildren } from 'react';

export const MyTooltip: FC<PropsWithChildren<TooltipProps>> = forwardRef((props, ref) => {
  const theme = useMantineTheme();
  return (
    <Tooltip color={theme.primaryColor} withArrow transition="slide-up" {...props}>
      {props.children}
    </Tooltip>
  );
});

MyTooltip.displayName = 'MyTooltip';
