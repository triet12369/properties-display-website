import { forwardRef, MouseEventHandler, Ref } from 'react';
import { Button, ButtonProps } from '@mantine/core';

type MyButtonProps = ButtonProps & {
  onClick?: MouseEventHandler;
};

export const MyButton = forwardRef(
  (props: MyButtonProps, ref: Ref<HTMLAnchorElement>) => {
    const { onClick } = props;

    return <Button ref={ref} component="a" onClick={onClick} {...props}></Button>;
  },
);

MyButton.displayName = 'MyButton';
