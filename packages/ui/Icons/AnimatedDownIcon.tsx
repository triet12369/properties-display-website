import { keyframes } from '@emotion/react';
import { createStyles } from '@mantine/core';
import { DoubleArrowDownIcon } from '@modulz/radix-icons';
import { FC, MouseEventHandler } from 'react';

type IAnimatedDownIconProps = {
  onClick?: MouseEventHandler<SVGElement>;
};

const bounce = keyframes`
  from {
    transform: translate3d(0,0,0);
  }

  50% {
    transform: translate3d(0,-15px,0);
  }

  to {
    transform: translate3d(0,0,0);
  }
`;

const fadingPulse = keyframes`
  from {
    opacity: 1;
  }

  50% {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const useStyles = createStyles((theme, props: IAnimatedDownIconProps) => {
  const bounceDuration = 1.5; // seconds
  const bounceTimes = 3;
  const fadeDuration = 3; // seconds
  return {
    icon: {
      color: theme.white,
      width: 40,
      height: 40,
      animation: `${bounce} ${bounceDuration}s ease 0s ${bounceTimes}, ${fadingPulse} ${fadeDuration}s ease ${
        bounceDuration * bounceTimes
      }s infinite`,
      '&:hover': {
        cursor: props.onClick ? 'pointer' : 'none',
      },
    },
  };
});

export const AnimatedDownIcon: FC<IAnimatedDownIconProps> = (props) => {
  const { classes } = useStyles(props);
  return <DoubleArrowDownIcon className={classes.icon} onClick={props?.onClick} />;
};
