import { Select, SelectProps } from '@mantine/core';
import { FC, useMemo } from 'react';

// TODO: Make this generic to support future years selections
const CURRENT_YEAR = new Date().getFullYear();
const NUM_YEARS = 100;
// const YEAR_RANGE = [...(Array(YEARS_RANGE).keys())].map((k) => k + (CURRENT_YEAR - YEARS_RANGE) + 1).reverse();

type YearSelectionProps = Omit<SelectProps, 'data'>;

export const YearSelection: FC<YearSelectionProps> = (props) => {
  return (
    <NumberSelection
      startNumber={CURRENT_YEAR}
      endNumber={CURRENT_YEAR - NUM_YEARS}
      {...props}
    />
  );
};

type NumberSelectionProps = Omit<SelectProps, 'data'> & {
  startNumber: number;
  endNumber: number;
  reverse?: boolean;
};

export const NumberSelection: FC<NumberSelectionProps> = (props) => {
  const { startNumber, endNumber, reverse, ...rest } = props;
  const NUMBER_RANGE = useMemo(() => {
    const numEntries = Math.abs(startNumber - endNumber) + 1;
    const arr = [...Array.from(Array(numEntries)).keys()].map((k) => {
      if (startNumber < endNumber) return k + startNumber;
      else return k + endNumber;
    });

    // if user supplies startNumber > endNumber, they want to display startNumber on top
    // since Array(numEntries).keys() only produces increasing series, we have to reverse it
    if (startNumber > endNumber) arr.reverse();

    // escape hatch for when the user wants to reverse again
    if (reverse) return arr.reverse();
    return arr;
  }, [endNumber, reverse, startNumber]);

  return (
    <Select
      data={NUMBER_RANGE.map((item) => ({
        label: String(item),
        value: item as unknown as string, // bypass Mantine's SelectItem force-string type
      }))}
      searchable
      nothingFound="Not found"
      {...rest}
    />
  );
};
