import { Title, TitleProps, createStyles, useMantineTheme } from '@mantine/core';
import { FC } from 'react';

type MyPageTitleProps = TitleProps;
const useStyles = createStyles((theme) => ({
  title: {
    letterSpacing: '0.1rem',
    fontWeight: 400,
    textTransform: 'uppercase'
  }
}));
export const MyPageTitle: FC<MyPageTitleProps> = (props) => {
  const { classes } = useStyles();
  const theme = useMantineTheme();
  return (
    <Title order={2} align="center" color={theme.primaryColor} className={classes.title} {...props} ></Title>
  );
};