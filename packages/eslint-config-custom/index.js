module.exports = {
  extends: [
    'next/core-web-vitals',
    'react-app',
    'plugin:react-hooks/recommended',
    'prettier',
  ],
  plugins: ['react-hooks', 'prettier'],
  rules: {
    '@next/next/no-html-link-for-pages': 'off',
    'react/jsx-key': 'off',
    quotes: ['warn', 'single'],
    semi: ['error', 'always'],
    indent: ['error', 2],
    'react/jsx-indent': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'prettier/prettier': [
      'error',
      {},
      {
        usePrettierrc: true,
      },
    ],
  },
};
