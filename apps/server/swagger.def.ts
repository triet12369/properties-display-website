import { swIndexRouter } from './routes/index';
import { swloginRouter } from './routes/login';
import { swPropertyRouter } from './routes/property/';
import { swPropertyListingsRouter } from './routes/property/search';
import { swPropertyImageRouter } from './routes/propertyImage';
import { swpropertyImageDataRouter } from './routes/propertyImage/data';
//import { swPostUser } from './routes/user/users';

const swagger = {
  openapi: '3.0.0',
  info: {
    title: 'Express API',
    version: '1.0.0',
    description: 'The REST API test service. lets document',
  },
  paths: {
    ...swIndexRouter,
    ...swPropertyRouter,
    ...swPropertyListingsRouter,
    ...swPropertyImageRouter,
    ...swpropertyImageDataRouter,
    ...swloginRouter,
  },
};

export default swagger;
