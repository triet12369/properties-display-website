import express, { Request, Response, NextFunction } from 'express';

const usersRouter = express.Router();

export const swPostUser = {
  summary: 'Create the new user',
  tags: ['User'],
  requestBody: {
    content: {
      'application/json': {
        schema: {},
      },
    },
  },
  responses: {
    '200': {
      description: 'User created',
    },
    default: {
      description: 'Error message',
    },
  },
};

usersRouter.get('/', function (req: Request, res: Response, next: NextFunction) {
  res.send('respond with a resource boiiiiii');
});

// --isolatedModules
// make this file a module
export default usersRouter;
