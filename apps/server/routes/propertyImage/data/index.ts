import express from 'express';
import { getImageDataById } from './propertyImage-data-get.route';

const propertyImageDataRouter = express.Router();

// here the our swagger info
export const swpropertyImageDataRouter = {
  '/propertyImage/data/{imageId}': {
    get: {},
  },
};

propertyImageDataRouter.get('/:imageId', getImageDataById);

// --isolatedModules
// make this file a module
export default propertyImageDataRouter;
