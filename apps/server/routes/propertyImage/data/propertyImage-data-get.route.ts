import { respondInternalError, respondNotFound } from '../../../utils/responses';
import { validateObjectId } from '../../../utils/validateObjectId';
import dbo from '../../../db/conn';
import { RouteFunction } from '../../../types';
import { GridFSFile, ObjectId } from 'mongodb';
// swagger info
export const swGetProperty = {
  summary: 'Get Image By Id',
  tags: ['Property Image'],
  responses: {
    '200': {
      description: 'Image buffer data',
      content: {
        // 'application/json': {
        //   'schema': {
        //     ...swaggerSchemas.PropertySchema_WithId,
        //   }
        // }
      },
    },
  },
};

export const getImageDataById: RouteFunction = async (req, res, next) => {
  const { params } = req;
  const { imageId } = params;
  if (!validateObjectId(imageId)) {
    respondNotFound(res);
    return;
  }
  const imageBucket = dbo.getPropertyImageBucket();
  try {
    const findResult = await imageBucket.find({ _id: new ObjectId(imageId) });
    const findResultArr = (await findResult.toArray()) as GridFSFile[];
    if (findResultArr.length) {
      const img = findResultArr[0];
      const mime = img.contentType;
      const filename = img.filename;
      res.set('Content-Type', mime);
      res.set('Content-Disposition', 'inline; filename=' + filename);
      const read_stream = imageBucket.openDownloadStream(new ObjectId(imageId));
      read_stream.pipe(res);
    } else respondNotFound(res);
  } catch (e) {
    console.log(e);
    respondInternalError(res, e);
  }
};
