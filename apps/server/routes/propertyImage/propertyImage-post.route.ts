import { PROPERTY_IMAGE } from './../../db/resources';
import { InsertOneResult, ObjectId } from 'mongodb';
import dbo from '../../db/conn';
import { PROPERTY } from '../../db/resources';
import { PropertyImage } from '../../types/schemaTypes';
import { respondInternalError, respondNotFound, validateObjectId } from '../../utils';
import { RouteFunction } from './../../types/index';
import { Readable } from 'stream';
import sizeOf from 'buffer-image-size';

export const createNewPropertyImages: RouteFunction = async (req, res, next) => {
  const { params } = req;
  const files = req.files as Express.Multer.File[];
  const { propertyId } = params;
  //   console.log('files', files);
  if (!files?.length) return;
  if (!validateObjectId(propertyId)) {
    respondNotFound(res);
    return;
  }

  const db = dbo.getDb();
  const propertyCollection = db.collection(PROPERTY);
  const propertyImageCollection = db.collection(PROPERTY_IMAGE);
  try {
    const findResult = await propertyCollection.findOne({
      _id: new ObjectId(propertyId),
    });
    if (!findResult) respondNotFound(res);
    else {
      // found object, we can upload images using this objectID now
      const imageBucket = dbo.getPropertyImageBucket();
      const insertResultPromises: Promise<InsertOneResult<Document>>[] = [];
      files.forEach((file: Express.Multer.File) => {
        if (file.mimetype.includes('image')) {
          // is images
          const imageId = new ObjectId();
          const dimensions = sizeOf(file.buffer);
          const imageMetadata: PropertyImage = {
            _id: imageId,
            propertyId,
            mimeType: file.mimetype,
            width: dimensions.width,
            height: dimensions.height,
          } as const;
          //   file.buffer
          const stream = Readable.from(file.buffer);
          stream.pipe(
            imageBucket.openUploadStream(imageId.toString(), {
              id: imageId,
              contentType: file.mimetype,
            }),
          );
          insertResultPromises.push(propertyImageCollection.insertOne(imageMetadata));
        }
      });

      const insertResults = await Promise.allSettled(insertResultPromises);
      res.send(insertResults);
    }
  } catch (e) {
    console.log(e);
    respondInternalError(res, e);
  }
};
