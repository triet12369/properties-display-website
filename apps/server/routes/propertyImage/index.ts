import express from 'express';
import multer from 'multer';
import { PROPERTY_IMAGE } from '../../db/resources';
import { authenticateToken } from '../../utils/authenticateToken';
import { deletePropertyImageById } from './propertyImage-delete.route';
import { createNewPropertyImages } from './propertyImage-post.route';

const propertyImageRouter = express.Router();
// setup image storage
const storage = multer.memoryStorage();
const mediaUploads = multer({ storage });

// here the our swagger info
export const swPropertyImageRouter = {
  '/propertyImage/upload': {
    post: {},
  },
};

propertyImageRouter.post(
  '/upload/:propertyId',
  mediaUploads.array(PROPERTY_IMAGE),
  authenticateToken,
  createNewPropertyImages,
);

propertyImageRouter.delete(
  '/:imageId',
  authenticateToken,
  deletePropertyImageById,
);

// --isolatedModules
// make this file a module
export default propertyImageRouter;
