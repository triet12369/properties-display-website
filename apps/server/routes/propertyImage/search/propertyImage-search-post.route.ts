import { PropertyImageFilterSchema } from './../../../db/schemas/PropertyImageSchema';
import { respondInternalError } from '../../../utils/responses';
import { validateSchemaOnRuntime } from '../../../utils/validateSchema';
import dbo from '../../../db/conn';
import { PROPERTY, PROPERTY_IMAGE } from '../../../db/resources';
import { RouteFunction } from '../../../types';
import { PropertyImage, PropertyImageFilter } from '../../../types/schemaTypes';
import { WithId } from 'mongodb';
import swaggerSchemas from '../../../db/swaggerSchemas';

// swagger info
export const swGetListings = {
  summary: `Retrieve the list of images for ${PROPERTY}`,
  tags: ['Property Images'],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...swaggerSchemas.PropertyImageFilterSchema,
        },
      },
    },
  },
  responses: {
    '200': {
      description: `List of image ids for that ${PROPERTY}`,
      content: {
        'application/json': {
          schema: {
            ...swaggerSchemas.PropertyImageSchema_ArrayWithId,
          },
        },
      },
    },
  },
};

const searchPropertyImages: RouteFunction = async function (req, res, next) {
  const { body } = req;
  const filter = await validateSchemaOnRuntime<PropertyImageFilter>(
    PropertyImageFilterSchema,
    body,
    res,
  );
  if (!filter) return;
  try {
    const db = dbo.getDb();
    const propertyImageCollection = db.collection(PROPERTY_IMAGE);
    const findResult = await propertyImageCollection.find(filter);
    const findResultArr = (await findResult.toArray()) as WithId<PropertyImage>[];

    // res.send(findResultArr.map((item) => item._id));
    res.send(findResultArr);
  } catch (err) {
    console.log('Error', err);
    respondInternalError(res, err);
  }
};

export default searchPropertyImages;
