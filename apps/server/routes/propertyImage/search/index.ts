import { PROPERTY_IMAGE } from './../../../db/resources';
import express from 'express';
import searchPropertyImages, { swGetListings } from './propertyImage-search-post.route';

const propertyImageSearchRouter = express.Router();

// here the our swagger info
export const swPropertyListingsRouter = {
  [`/${PROPERTY_IMAGE}/search`]: {
    post: {
      ...swGetListings,
    },
  },
};

propertyImageSearchRouter.post('/', searchPropertyImages);

export default propertyImageSearchRouter;
