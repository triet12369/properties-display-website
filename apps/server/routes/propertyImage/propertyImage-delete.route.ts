import { PROPERTY_IMAGE } from './../../../web/api/resources';
import { respondInternalError, respondNotFound } from './../../utils/responses';
import { validateObjectId } from './../../utils/validateObjectId';
import dbo from '../../db/conn';
import { RouteFunction } from '../../types';
import { ObjectId } from 'mongodb';

// swagger info
export const swDeleteImageProperty = {
  summary: 'Deletes the property image entry that matches the id',
  tags: ['Property'],
  responses: {
    '200': {
      description: 'returns the status of deletion and the count of documents deleted',
    },
  },
};

export const deletePropertyImageById: RouteFunction = async (req, res, next) => {
  const { params } = req;
  const { imageId } = params;
  if (!validateObjectId(imageId)) {
    respondNotFound(res);
    return;
  }
  const db = dbo.getDb();
  const propertyImageCollection = db.collection(PROPERTY_IMAGE);
  try {
    const imageIdObjectId = new ObjectId(imageId);
    const imageBucket = dbo.getPropertyImageBucket();
    const deleteResult = await propertyImageCollection.deleteOne({
      _id: imageIdObjectId,
    });
    //console.log('params', params);
    if (!deleteResult) respondNotFound(res);
    else {
      // delete image entry succeeded, now delete image binary
      await imageBucket.delete(imageIdObjectId);
      res.send({
        'deleteResult': deleteResult.acknowledged,
        'count': deleteResult.deletedCount,
      });
    }
  } catch (e) {
    console.log(e);
    respondInternalError(res, e);
  }
};
