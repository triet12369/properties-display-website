import { respondInternalError, respondNotFound } from './../../utils/responses';
import { validateObjectId } from './../../utils/validateObjectId';
import { PROPERTY } from './../../db/resources';
import dbo from '../../db/conn';
import swaggerSchemas from '../../db/swaggerSchemas';
import { RouteFunction } from '../../types';
import { ObjectId } from 'mongodb';

// swagger info
export const swGetProperty = {
  summary: 'Get Property By Id',
  tags: ['Property'],
  responses: {
    '200': {
      description: 'Object Property with Id',
      content: {
        'application/json': {
          schema: {
            ...swaggerSchemas.PropertySchema_WithId,
          },
        },
      },
    },
  },
};

export const getPropertyById: RouteFunction = async (req, res, next) => {
  const { params } = req;
  const { propertyId } = params;
  if (!validateObjectId(propertyId)) {
    respondNotFound(res);
    return;
  }
  const db = dbo.getDb();
  const propertyCollection = db.collection(PROPERTY);
  try {
    const findResult = await propertyCollection.findOne({
      _id: new ObjectId(propertyId),
    });
    if (!findResult) respondNotFound(res);
    else res.send(findResult);
  } catch (e) {
    console.log(e);
    respondInternalError(res, e);
  }
};
