import { Property } from '../../types';
import { respondInternalError } from './../../utils/responses';
import { RouteFunction } from './../../types/index';
import { PropertySchema } from '../../db/schemas/PropertySchema';
import swaggerSchemas from '../../db/swaggerSchemas';
import { validateSchemaOnRuntime } from '../../utils/';
import dbo from '../../db/conn';
import { PROPERTY } from '../../db/resources';

// swagger info
export const swPostProperty = {
  summary: 'Create new property',
  tags: ['Property'],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...swaggerSchemas.PropertySchema,
        },
      },
    },
  },
  responses: {
    '200': {
      description: 'Returns created property with id',
      content: {
        'application/json': {
          schema: {
            ...swaggerSchemas.PropertySchema_WithId,
          },
        },
      },
    },
    default: {
      description: 'Error message',
    },
  },
};

export const createNewProperty: RouteFunction = async (req, res) => {
  const { body } = req;
  const validatedData = await validateSchemaOnRuntime<Property>(
    PropertySchema,
    body,
    res,
  );
  if (!validatedData) return;
  try {
    // validation success
    const db = dbo.getDb();
    const propertyCollection = db.collection(PROPERTY);
    const insertResult = await propertyCollection.insertOne(validatedData);
    console.log('Insert successful', insertResult);
    res.send({
      _id: insertResult.insertedId,
      ...validatedData,
    });
  } catch (err) {
    console.log('Error', err);
    respondInternalError(res, err);
  }
};
