import { PROPERTY } from './../../../db/resources';
import express from 'express';
import searchProperties, { swGetListings } from './property-search-post.route';

const propertySearchRouter = express.Router();

// here the our swagger info
export const swPropertyListingsRouter = {
  [`/${PROPERTY}/search`]: {
    post: {
      ...swGetListings,
    },
  },
};

propertySearchRouter.post('/', searchProperties);

export default propertySearchRouter;
