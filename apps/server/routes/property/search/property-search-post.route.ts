import { respondInternalError } from './../../../utils/responses';
import { validateSchemaOnRuntime } from './../../../utils/validateSchema';
import dbo from '../../../db/conn';
import { PROPERTY } from '../../../db/resources';
import { RouteFunction } from '../../../types';
import { PropertyFilterSchema } from '../../../db/schemas';
import { Property, PropertyFilter } from '../../../types/schemaTypes';
import { PROPERTY_SORTS, PROPERTY_TYPES } from '../../../constants';
import { WithId } from 'mongodb';
import swaggerSchemas from '../../../db/swaggerSchemas';

// swagger info
export const swGetListings = {
  summary: `Retrieve the list of filtered ${PROPERTY}`,
  tags: ['Property'],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...swaggerSchemas.PropertyFilterSchema,
        },
      },
    },
  },
  responses: {
    '200': {
      description: `List of filtered ${PROPERTY}`,
      content: {
        'application/json': {
          schema: {
            ...swaggerSchemas.PropertySchema_ArrayWithId,
          },
        },
      },
    },
  },
};

type PropertyMongoQueryFilter = Omit<PropertyFilter, 'priceRange' | 'sort'> & {
  [key: string]: any;
};

const searchProperties: RouteFunction = async function (req, res, next) {
  const { body } = req;
  const filter = await validateSchemaOnRuntime<PropertyFilter>(
    PropertyFilterSchema,
    body,
    res,
  );
  if (!filter) return;
  try {
    const db = dbo.getDb();
    const propertyCollection = db.collection(PROPERTY);
    const { priceRange, sort, types, ...rest } = filter;

    const mongoQuery: PropertyMongoQueryFilter = {
      ...rest,
    };

    // build range query depending on property type
    if (priceRange) {
      if (filter.type === PROPERTY_TYPES.Rental) {
        mongoQuery['pricing.rentalPrice'] = {
          $gte: priceRange.from,
          $lte: priceRange.to,
        };
      } else
        mongoQuery['pricing.salesPrice'] = {
          $gte: priceRange.from,
          $lte: priceRange.to,
        };
    }

    // perform query
    let result;
    if (types && types.length) {
      const promises = types.map((type) =>
        propertyCollection.find<Property>({ ...mongoQuery, type }).toArray(),
      );
      const res = await Promise.all(promises);
      const flattenedRes = res.flat();
      result = flattenedRes;
    } else {
      const findResult = propertyCollection.find(mongoQuery);
      const findResultArr = (await findResult.toArray()) as WithId<Property>[];
      result = findResultArr;
    }

    // sort data by filter criteria
    if (sort) {
      const { order } = sort;
      switch (sort.field) {
        case PROPERTY_SORTS.BY_RENTAL_PRICE:
          result.sort((a, b) => {
            const aPrice: number = a.pricing?.rentalPrice || 0;
            const bPrice: number = b.pricing?.rentalPrice || 0;
            return order === 'descending' ? bPrice - aPrice : aPrice - bPrice;
          });
          break;
        case PROPERTY_SORTS.BY_SALES_PRICE:
          result.sort((a, b) => {
            const aPrice: number = a.pricing?.salesPrice || 0;
            const bPrice: number = b.pricing?.salesPrice || 0;
            return order === 'descending' ? bPrice - aPrice : aPrice - bPrice;
          });
          break;
        case PROPERTY_SORTS.BY_BUILD_YEAR:
          result.sort((a, b) => {
            const aYear: number = a.builtYear || 0;
            const bYear: number = b.builtYear || 0;
            return order === 'descending' ? bYear - aYear : aYear - bYear;
          });
          break;
        case PROPERTY_SORTS.BY_AREA:
          result.sort((a, b) => {
            const aArea: number = (a.interiorArea || 0) + (a.propertyArea || 0);
            const bArea: number = (b.interiorArea || 0) + (b.propertyArea || 0);
            return order === 'descending' ? bArea - aArea : aArea - bArea;
          });
          break;
        case PROPERTY_SORTS.BY_BED:
          result.sort((a, b) => {
            const aBeds: number = (a.fullBedCount || 0) + (a.halfBedCount || 0) / 2;
            const bBeds: number = (b.fullBedCount || 0) + (b.halfBedCount || 0) / 2;
            return order === 'descending' ? bBeds - aBeds : aBeds - bBeds;
          });
          break;
        case PROPERTY_SORTS.BY_BATH:
          result.sort((a, b) => {
            const aBaths: number = (a.fullBathCount || 0) + (a.halfBathCount || 0) / 2;
            const bBaths: number = (b.fullBathCount || 0) + (b.halfBathCount || 0) / 2;
            return order === 'descending' ? bBaths - aBaths : aBaths - bBaths;
          });
          break;
        default:
          break;
      }
    }

    // // sleep for testing
    // await new Promise((resolve) => {
    //   setTimeout(resolve, 10000);
    // });
    res.send(result);
  } catch (err) {
    console.log('Error', err);
    respondInternalError(res, err);
  }
};

export default searchProperties;
