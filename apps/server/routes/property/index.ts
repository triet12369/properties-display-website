import { authenticateToken } from './../../utils/authenticateToken';
import express from 'express';
import { deletePropertyById, swDeleteProperty } from './property-delete.route';
import { getPropertyById, swGetProperty } from './property-get.route';
import { updateProperty } from './property-update.route';
import { createNewProperty, swPostProperty } from './property-post.route';

const propertyRouter = express.Router();

// here the our swagger info
export const swPropertyRouter = {
  '/property': {
    post: {
      ...swPostProperty,
    },
  },
  '/property/{propertyId}': {
    get: {
      ...swGetProperty,
    },
    delete: {
      ...swDeleteProperty,
    },
  },
};

// /property/123 --> propertyId = 123
propertyRouter.get('/:propertyId', getPropertyById);

propertyRouter.post('/', authenticateToken, createNewProperty);

propertyRouter.delete('/:propertyId', authenticateToken, deletePropertyById);

propertyRouter.patch('/:propertyId', authenticateToken, updateProperty);

// --isolatedModules
// make this file a module
export default propertyRouter;
