import { respondInternalError, respondNotFound } from './../../utils/responses';
import { RouteFunction } from './../../types/index';
import { PropertyPatchSchema } from '../../db/schemas/PropertySchema';
import swaggerSchemas from '../../db/swaggerSchemas';
import { validateSchemaOnRuntime } from '../../utils/';
import dbo from '../../db/conn';
import { PROPERTY } from '../../db/resources';
import { ObjectId } from 'mongodb';
import { mergeDeep } from '../../utils/mergeDeep';
import { z } from 'zod';

// swagger info
export const swUpdateProperty = {
  summary: 'Update existing property',
  tags: ['Property'],
  requestBody: {
    content: {
      'application/json': {
        schema: {
          ...swaggerSchemas.PropertyPatchSchema,
        },
      },
    },
  },
  responses: {
    '200': {
      description: 'Returns update result and updated property with id',
      content: {
        'application/json': {
          schema: {
            ...swaggerSchemas.PropertySchema_WithId,
          },
        },
      },
    },
    default: {
      description: 'Error message',
    },
  },
};

export const updateProperty: RouteFunction = async (req, res) => {
  const { params } = req;
  const { propertyId } = params;
  const { body } = req;

  const validatedData = await validateSchemaOnRuntime<z.infer<typeof PropertyPatchSchema>>(PropertyPatchSchema, body, res);
  if (!validatedData) return;

  try {
    const db = dbo.getDb();
    const propertyCollection = db.collection(PROPERTY);
    const findResult = await propertyCollection.findOne({
      _id: new ObjectId(propertyId),
    });
    if (!findResult) respondNotFound(res);

    const target = findResult;
    //insert hours of sadness here
    const source = body;

    const mergeDeepResult = mergeDeep(target, source);

    const update = { $set: mergeDeepResult };
    const options = { upsert: false };
    const updateResult = await propertyCollection.updateOne(
      { _id: new ObjectId(propertyId) },
      update,
      options,
    );

    if (updateResult.acknowledged) {
      res.send(mergeDeepResult);
    } else {
      res.send({
        updateResult,
      });
    }
  } catch (e) {
    console.log(e);
    respondInternalError(res, e);
  }
};
