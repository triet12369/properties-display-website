import { respondInternalError, respondNotFound } from './../../utils/responses';
import { validateObjectId } from './../../utils/validateObjectId';
import { PROPERTY } from './../../db/resources';
import dbo from '../../db/conn';
import { RouteFunction } from '../../types';
import { ObjectId } from 'mongodb';

// swagger info
export const swDeleteProperty = {
  summary: 'Deletes the property information entry that matches the id',
  tags: ['Property'],
  responses: {
    '200': {
      description: 'returns the status of deletion and the count of documents deleted',
    },
  },
};

export const deletePropertyById: RouteFunction = async (req, res, next) => {
  const { params } = req;
  const { propertyId } = params;
  if (!validateObjectId(propertyId)) {
    respondNotFound(res);
    return;
  }
  const db = dbo.getDb();
  const propertyCollection = db.collection(PROPERTY);
  try {
    const deleteResult = await propertyCollection.deleteOne({
      _id: new ObjectId(propertyId),
    });
    //console.log('params', params);
    if (!deleteResult) respondNotFound(res);
    else
      res.send({
        'deleteResult': deleteResult.acknowledged,
        'count': deleteResult.deletedCount,
      });
  } catch (e) {
    console.log(e);
    respondInternalError(res, e);
  }
};
