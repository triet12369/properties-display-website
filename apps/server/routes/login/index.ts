import express from 'express';
import { login, swPostLogin } from './login-post.route';

const loginRouter = express.Router();

// here the our swagger info
export const swloginRouter = {
  '/login': {
    post: {
      ...swPostLogin,
    },
  },
//   '/login/me': {
//     get: {
//       ...swGetProperty,
//     },
//   },
};

// /property/123 --> propertyId = 123
loginRouter.post('/', login);

// loginRouter.get('/me', refreshToken);

// --isolatedModules
// make this file a module
export default loginRouter;
