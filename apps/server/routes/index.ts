import express, { Request, Response, NextFunction } from 'express';

// here the our swagger info
export const swIndexRouter = {
  '/': {
    get: {
      summary: 'Home Page',
      description: 'Optional extended description in CommonMark or HTML.',
      responses: {
        '200': {
          description: 'Generic HTML page',
        },
      },
    },
  },
};

//figure out how to integrate data types

const indexRouter = express.Router();

indexRouter.get('/', function (req: Request, res: Response, next: NextFunction) {
  res.render('index', { title: 'Property Display Website' });
});

// --isolatedModules
// make this file a module
export default indexRouter;
