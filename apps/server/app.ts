import express, { Request, Response, NextFunction } from 'express';
// import { convertFromDirectory } from 'joi-to-typescript';
import path from 'path';
import indexRouter from './routes/index';
import usersRouter from './routes/user/users';
import propertyRouter from './routes/property/';
import propertyListingsRouter from './routes/property/search';
import whiteList from './config/whitelist';
import swDocument from './swagger.def';
import { PROPERTY, PROPERTY_IMAGE } from './db/resources';
import dbo from './db/conn';
import propertyImageRouter from './routes/propertyImage';
import propertyImageSearchRouter from './routes/propertyImage/search';
import propertyImageDataRouter from './routes/propertyImage/data';
import loginRouter from './routes/login';

const createError = require('http-errors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');

//const swaggerJsdoc = require('swagger-jsdoc');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
  cors({
    origin: whiteList,
  }),
);

// setup swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swDocument));

// configure list of endpoints
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/login', loginRouter);
app.use(`/${PROPERTY}`, propertyRouter);
app.use(`/${PROPERTY}/search`, propertyListingsRouter);
app.use(`/${PROPERTY_IMAGE}`, propertyImageRouter);
app.use(`/${PROPERTY_IMAGE}/search`, propertyImageSearchRouter);
app.use(`/${PROPERTY_IMAGE}/data`, propertyImageDataRouter);

// catch 404 and forward to error handler
app.use(function (req: Request, res: Response, next: NextFunction) {
  next(createError(404));
});

// error handler
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

process.on('SIGTERM', function () {
  // clean up
  dbo.close();
  console.log('Closed connection');
});

// --isolatedModules
// make this file a module
export default app;
