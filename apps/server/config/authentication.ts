export const AUTH_EXPIRE_TIME = 43200;

export const JWT_SECRET = process.env.JWT_SECRET as string;