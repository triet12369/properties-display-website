export enum PROPERTY_SORTS {
  BY_RENTAL_PRICE = 'rentalPrice',
  BY_SALES_PRICE = 'salesPrice',
  BY_BED = 'bed',
  BY_BATH = 'bath',
  BY_AREA = 'area',
  BY_BUILD_YEAR = 'builtYear',
}
