export const HIGHLIGHTS_BASE_CHOICES = [
  'Spacious',
  'Close to school',
  'Low crime rate',
  'Quiet neighborhood',
  'Wood burning fireplace',
  'Great room',
  'Separate outside access',
];
