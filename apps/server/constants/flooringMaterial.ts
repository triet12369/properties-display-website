export enum FLOORING_MATERIALS {
  Carpet = 'Carpet',
  Ceramic = 'Ceramic',
  Wood = 'Wood',
}
