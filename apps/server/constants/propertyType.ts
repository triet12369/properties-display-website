export enum PROPERTY_TYPES {
  Sale = 'Sale',
  Rental = 'Rental',
  Both = 'Both',
}
