/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
import { PropertySchema, PropertyPatchSchema } from '../db/schemas/PropertySchema';

export function isObject(item: any) {
  return item && typeof item === 'object' && !Array.isArray(item);
}

/**
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
export function mergeDeep(target: any, ...sources: any): any {
  //base case
  if (!sources.length) return target;

  //get first element and sets sources to reduced object
  const source = sources.shift();

  //check if extracted element is object
  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        mergeDeep(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    }
  }
  return mergeDeep(target, ...sources);
}
