import jwt from 'jsonwebtoken';
import { AUTH_EXPIRE_TIME, JWT_SECRET } from '../config/authentication';


export const generateAccessToken = (username: string) => {
  return jwt.sign({ username }, JWT_SECRET, { expiresIn: AUTH_EXPIRE_TIME });
};