import { Response } from 'express';
import { StatusCodes, ReasonPhrases } from 'http-status-codes';

export const respondGeneric = (res: Response, code: StatusCodes, body: any) => {
  res.status(code).send(body);
};

export const respondNotFound = (res: Response) => {
  respondGeneric(res, StatusCodes.NOT_FOUND, ReasonPhrases.NOT_FOUND);
};

export const respondBadRequest = (res: Response, err: any) => {
  respondGeneric(res, StatusCodes.BAD_REQUEST, err);
};

export const respondInternalError = (res: Response, err: any) => {
  respondGeneric(res, StatusCodes.INTERNAL_SERVER_ERROR, err);
};

export const respondUnauthorized = (res: Response, err: any) => {
  respondGeneric(res, StatusCodes.UNAUTHORIZED, err);
};
