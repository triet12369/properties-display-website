import { JWT_SECRET } from './../config/authentication';
import { RouteFunction } from './../types/index';
import jwt from 'jsonwebtoken';
import { respondUnauthorized } from './responses';

export const authenticateToken: RouteFunction = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) return respondUnauthorized(res, 'No token provided.');

  jwt.verify(token, JWT_SECRET, (err: any, user: any) => {
    if (err) return respondUnauthorized(res, err);


    next();
  });
};