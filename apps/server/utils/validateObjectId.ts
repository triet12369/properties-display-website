export const validateObjectId = (input: string) => {
  if (input.match(/^[0-9a-fA-F]{24}$/)) {
    // it's an ObjectID
    return true;
  } else {
    // nope
    return false;
  }
};
