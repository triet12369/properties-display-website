import { ObjectId } from 'mongodb';
import dbo from '../db/conn';
import { RouteFunction } from './../types/index';
import { respondNotFound, respondInternalError } from './responses';
import { validateObjectId } from './validateObjectId';

const getResourceById = (resource: string, fieldName: string): RouteFunction => {
  return async (req, res, next) => {
    const { params } = req;
    const id = params[fieldName];
    if (!validateObjectId(id)) {
      respondNotFound(res);
      return;
    }
    const db = dbo.getDb();
    const propertyCollection = db.collection(resource);
    try {
      const findResult = await propertyCollection.findOne({
        _id: new ObjectId(id),
      });
      if (!findResult) respondNotFound(res);
      else res.send(findResult);
    } catch (e) {
      console.log(e);
      respondInternalError(res, e);
    }
  };
};

export default getResourceById;
