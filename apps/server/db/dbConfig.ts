export const DATABASES = {
  MAIN: 'propertyManagement',
};

export const BUCKETS = {
  PROPERTY_IMAGES: 'propertyImages',
};
