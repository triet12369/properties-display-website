import * as schemas from './schemas';
import { generateSchema } from '@anatine/zod-openapi';
import { z } from 'zod';

const swaggerSchemas: { [key: string]: any } = {};

Object.entries(schemas).forEach(([key, val]) => {
  const schemaWithId = val.merge(
    z.object({
      _id: z.string(),
    }),
  );
  swaggerSchemas[key] = generateSchema(val);
  swaggerSchemas[`${key}_Array`] = generateSchema(z.array(val));
  swaggerSchemas[`${key}_ArrayWithId`] = generateSchema(z.array(schemaWithId));
  swaggerSchemas[`${key}_WithId`] = generateSchema(schemaWithId);
});

export default swaggerSchemas;
