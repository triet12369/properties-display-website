// make connection to our MongoDB instance
import { Db, GridFSBucket, MongoClient, ServerApiVersion } from 'mongodb';
import { BUCKETS, DATABASES } from './dbConfig';
import getDBCOnnectionString from './getDBConnectionString';

// const { GridFsStorage } = require('multer-gridfs-storage');

export const connectionOptions = { serverApi: ServerApiVersion.v1 };
const client = new MongoClient(getDBCOnnectionString(), connectionOptions);

let database: Db;
let propertyImageBucket: GridFSBucket;

const dbo = {
  connectToServer: async function (errCallback: Function) {
    try {
      await client.connect();
      // Establish and verify connection
      database = await client.db(DATABASES.MAIN);
      console.log('Successfully connected to MongoDB.');
      // const collections = await database.collections();
      // connect or initialize property image bucket
      propertyImageBucket = new GridFSBucket(database, {
        bucketName: BUCKETS.PROPERTY_IMAGES,
      });
    } catch (e) {
      errCallback(e);
    }
  },

  getDb: function () {
    return database;
  },

  getPropertyImageBucket: function () {
    return propertyImageBucket;
  },

  close: function () {
    client.close();
  },
};

// --isolatedModules
// make this file a module
export default dbo;
