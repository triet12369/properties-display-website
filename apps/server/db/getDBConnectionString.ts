import dotenv from 'dotenv';

const getDBCOnnectionString = () => {
  dotenv.config(); // load .env file and initialize environment constants
  const connectionString = process.env.DB_CONN as string;
  // console.log('connectionString', connectionString);
  // const uri = `mongodb+srv://${connectionString}@propertydisplaydb.3yrtptw.mongodb.net/?retryWrites=true&w=majority`;
  return connectionString;
};

export default getDBCOnnectionString;
