import { User } from '../../types/schemaTypes';

const SEED_USERS: User[] = [
  {
    userId: 'triet.cao.dev@gmail.com',
    contact: null
  },
  {
    userId: 'menuwan.wijesinghe@gmail.com',
    contact: null
  }
];

export default SEED_USERS;
