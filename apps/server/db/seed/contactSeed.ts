import { Contact } from '../../types/schemaTypes';

const contacts: Contact[] = [
  {
    name: 'Tam Nguyen',
    address: 'Lincoln, NE 68516',
    phoneNumber: '4024804500',
    email: null,
  },
];

export default contacts;
