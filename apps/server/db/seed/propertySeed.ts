/* eslint-disable quotes */
import { PROPERTY_TYPES } from '../../constants';
import { HIGHLIGHTS_BASE_CHOICES } from '../../constants/highlightsChoices';
import { Property } from '../../types/schemaTypes';
import contacts from './contactSeed';

const getRandomHighlights = (): string[] => {
  const numHighlights = Math.floor(Math.random() * HIGHLIGHTS_BASE_CHOICES.length) || 1;
  const res = HIGHLIGHTS_BASE_CHOICES.slice();
  for (let i = 0; i < numHighlights; ++i) {
    const randomId = Math.floor(Math.random() * HIGHLIGHTS_BASE_CHOICES.length);
    res.splice(randomId, 1);
  }
  return res;
};

const SEED_PROPERTIES: Property[] = [
  {
    isActive: true,
    isPopular: true,
    title: 'Very nice 3 beds, all brick',
    type: PROPERTY_TYPES.Sale,
    location: {
      address: 'Very nice 3 bed, all brick ranch',
      city: 'Lincoln',
      zip: 68506,
      state: 'NE',
    },
    description:
      'Mostly brick, 3-bedroom, 1.75 bath, Garage, full basement,Sunroom, Nice and big back yard, home on cul-de-sac, New Roof in 2012, New central air and heat 2011, ceiling fans, some hardwood floors under carpet. covered porch in front. Rent: $1350 Price to sale: $145,000',
    pricing: {
      rentalPrice: 1350,
      rentalDurationDivider: 'perMonth',
      salesPrice: 145000,
    },
    fullBedCount: 3,
    fullBathCount: 2,
    halfBathCount: null,
    halfBedCount: null,
    builtYear: null,
    remodeledYear: null,
    propertyArea: null,
    interiorArea: null,
    flooringMaterials: null,
    acTypes: null,
    parking: null,
    appliances: null,
    contacts: [contacts[0]],
    highlights: getRandomHighlights(),
    neighborhoodDetails: null,
    hoaInformation: null,
    otherInformation: null,
  },
  {
    isActive: true,
    isPopular: true,
    title: 'Nice 3 bedrooms, 1.5 baths, ranch',
    type: PROPERTY_TYPES.Rental,
    location: {
      address: '7730 Mesa rd',
      city: 'Lincoln',
      zip: 68506,
      state: 'NE',
    },
    description:
      'If you are looking a sweet home for your family, here it is, ranch style, very clean house, 1066 SF that is not include a full basement. 3 bed, 1.5 bath, nice and quiet area, corner lot. Over sized 1 stall garage. The house is in the Meadowlane neighborhood in Lincoln, NE and close to great schools.',
    pricing: {
      rentalPrice: 1300,
      rentalDurationDivider: 'perMonth',
      salesPrice: 141000,
    },
    fullBedCount: 3,
    fullBathCount: 2,
    builtYear: 1966,
    halfBathCount: null,
    halfBedCount: null,
    remodeledYear: null,
    propertyArea: null,
    interiorArea: null,
    flooringMaterials: null,
    acTypes: null,
    parking: null,
    appliances: null,
    contacts: [contacts[0]],
    highlights: getRandomHighlights(),
    neighborhoodDetails: null,
    hoaInformation: null,
    otherInformation: null,
  },
  {
    isActive: true,
    isPopular: true,
    title: 'Nice 4 bedrooms, 2 baths, ranch',
    type: PROPERTY_TYPES.Rental,
    location: {
      address: '4316 Bel Ridge Dr',
      city: 'Lincoln',
      zip: 68521,
      state: 'NE',
    },
    description:
      "Just what you've been waiting for, a lovely ranch in Belmont area! This house has 4 bedrooms and 2 full bathrooms with a jetted tub in newly remodeled basement bathroom. The house has been very well maintained with new siding in 2013, new roof in 2010, New Patio Door in 2013, New Living Room window in 2013, updated bathroom in basement. The house has a 2 stall attached garage with a 220 amp circuit already in place. From the dining area is a deck and patio overlooking a large backyard with a forest in the background!",
    pricing: {
      rentalPrice: 1350,
      rentalDurationDivider: 'perMonth',
      salesPrice: 149000,
    },
    fullBedCount: 4,
    fullBathCount: 2,
    builtYear: 1967,
    contacts: [contacts[0]],
    halfBathCount: null,
    halfBedCount: null,
    remodeledYear: null,
    propertyArea: null,
    interiorArea: null,
    flooringMaterials: null,
    acTypes: null,
    parking: null,
    appliances: null,
    highlights: getRandomHighlights(),
    neighborhoodDetails: null,
    hoaInformation: null,
    otherInformation: null,
  },
  {
    isActive: true,
    isPopular: false,
    title: '12 Plex apartment in Down town Lincoln',
    type: PROPERTY_TYPES.Sale,
    location: {
      address: '1218 E St',
      city: 'Lincoln',
      zip: 68502,
      state: 'NE',
    },
    description:
      'Super cash flow! This three stories apartment building is located on E st and 13 st in down town of Lincoln, Nebraska; Close to restaurant, gas station, sport center, police station and etc. off road parking, 12 spaces. All electric. $425 for 1 bedroom, $550 for 2 bedrooms.',
    pricing: {
      salesPrice: 400000,
    },
    builtYear: 1974,
    fullBedCount: null,
    fullBathCount: null,
    halfBathCount: null,
    halfBedCount: null,
    remodeledYear: null,
    propertyArea: null,
    interiorArea: null,
    flooringMaterials: null,
    acTypes: null,
    parking: null,
    appliances: null,
    contacts: [contacts[0]],
    highlights: getRandomHighlights(),
    neighborhoodDetails: null,
    hoaInformation: null,
    otherInformation: null,
  },
  {
    isActive: true,
    isPopular: false,
    title: 'Big brick ranch, downtown',
    type: PROPERTY_TYPES.Rental,
    location: {
      address: '1300 A St',
      city: 'Lincoln',
      zip: 68502,
      state: 'NE',
    },
    description:
      'If you are looking house in downtown for your family. Huge ranch 3+ bedrooms, 2.5 baths at the corner lot. More than 2000 SF at main floor and about 1000 SF finish at basement. Basement has kitchen, wet bar and 1/2 bath. One garage and driveway can park more than 6 cars All brick. Heat pump. Two fire places.',
    pricing: {
      salesPrice: 149000,
      rentalPrice: 1350,
      rentalDurationDivider: 'perMonth',
    },
    builtYear: 1952,
    fullBedCount: null,
    fullBathCount: null,
    halfBathCount: null,
    halfBedCount: null,
    remodeledYear: null,
    propertyArea: null,
    interiorArea: null,
    flooringMaterials: null,
    acTypes: null,
    parking: null,
    appliances: null,
    contacts: [contacts[0]],
    highlights: getRandomHighlights(),
    neighborhoodDetails: null,
    hoaInformation: null,
    otherInformation: null,
  },
];
export default SEED_PROPERTIES;
