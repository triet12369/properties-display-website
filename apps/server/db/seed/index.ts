import { DATABASES } from './../dbConfig';
import { Db, MongoClient } from 'mongodb';
import { connectionOptions } from '../conn';
import getDBCOnnectionString from '../getDBConnectionString';
import { PROPERTY, USERS } from '../resources';
import SEED_PROPERTIES from './propertySeed';
import SEED_USERS from './userSeed';

// Clear the current DB and insert seed data
console.log('Reseting Server database...');

const client = new MongoClient(getDBCOnnectionString(), connectionOptions);

const connect = async () => {
  const database = await client.connect();
  const db = database.db('admin');
  const adminDb = db.admin();
  const databases = await adminDb.listDatabases();
  console.log('databases', databases);
  const blacklistDbs = ['admin', 'local', 'config']; // don't touch these databases
  const dbCollection: DbCollection = {};

  databases.databases.forEach((db) => {
    if (!blacklistDbs.some((dbName) => dbName === db.name)) {
      dbCollection[db.name] = database.db(db.name);
    }
  });
  // clear DB
  const promises = Object.keys(dbCollection).map((key) => {
    console.log('dropping database', dbCollection[key]);
    return dbCollection[key].dropDatabase();
  });

  const res = await Promise.all(promises);
  console.log('Drop databases result: ', res);

  const newDBCollection: DbCollection = {};
  Object.entries(DATABASES).forEach(([key, val]) => {
    newDBCollection[val] = database.db(val);
  });

  // try {
  //   // clear storage buckets
  //   const bucket = new GridFSBucket(dbCollection[DATABASES.MAIN], { bucketName: BUCKETS.PROPERTY_IMAGES });
  //   if (bucket) await bucket.drop();
  // } catch (e) {
  //   console.log(e);
  // }

  return dbCollection;
};

const seedProperty = async (db: Db) => {
  console.log(`Seeding ${PROPERTY}...`);
  const propertyCollection = await db.createCollection(PROPERTY);
  if (propertyCollection) {
    await propertyCollection.insertMany(SEED_PROPERTIES);
  }
  console.log(`Seeding ${USERS}...`);
  const userCollection = await db.createCollection(USERS);
  if (userCollection) {
    await userCollection.insertMany(SEED_USERS);
  }
};

connect()
  .then(async (dbCollection: DbCollection) => {
    // perform seeding
    await seedProperty(dbCollection[DATABASES.MAIN]);

    client.close();
  })
  .catch((e) => client.close());
export {};

type DbCollection = {
  [key: string]: Db;
};
