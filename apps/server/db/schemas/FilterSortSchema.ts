import { z } from 'zod';

export const FilterSortSchema = z.object({
  order: z.union([z.literal('ascending'), z.literal('descending')]).default('ascending'),
  field: z.string().optional(),
});
