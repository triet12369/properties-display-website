import { z } from 'zod';

export const ContactSchema = z.object({
  name: z.string(),
  phoneNumber: z.string().nullable().default(null),
  email: z.string().nullable().default(null),
  address: z.string().nullable().default(null),
});
