import { APPLIANCES } from './../../constants/appliances';
import { z } from 'zod';
import { AC_TYPES } from '../../constants/acType';
import {
  FLOORING_MATERIALS,
  PROPERTY_TYPES,
  RENTAL_DURATION_DIVIDERS,
  US_STATES,
} from '../../constants/index';
import { ContactSchema } from './ContactSchema';
import { FilterSortSchema } from './FilterSortSchema';
import { PROPERTY_SORTS } from '../../constants/propertySorts';

export const PropertyLocationSchema = z.object({
  address: z.string().trim().min(1),
  zip: z.number(),
  city: z.string().trim().min(1),
  state: z.enum(US_STATES),
});

export const PropertyPricingSchema = z.object({
  salesPrice: z.number().optional(),
  rentalPrice: z.number().optional(),
  rentalDurationDivider: z.enum(RENTAL_DURATION_DIVIDERS).optional(),
});

export const PropertyPriceRange = z.object({
  from: z.number().default(0),
  to: z.number(),
});

export const PropertyParkingSchema = z.object({
  uncoveredSpots: z.number(),
  coveredSpots: z.number(),
  features: z.array(z.string().trim().min(1)),
});

export const PropertySchema = z.object({
  isActive: z.boolean().default(false),
  isPopular: z.boolean().default(false),
  title: z.string().trim().optional(),
  location: PropertyLocationSchema,
  type: z.nativeEnum(PROPERTY_TYPES),
  pricing: PropertyPricingSchema.optional().nullable().default(null),
  fullBedCount: z.number().optional().nullable().default(null),
  fullBathCount: z.number().optional().nullable().default(null),
  halfBedCount: z.number().optional().nullable().default(null),
  halfBathCount: z.number().optional().nullable().default(null),
  description: z.string().trim().optional().nullable().default(null),
  contacts: z.array(ContactSchema).default([]),
  builtYear: z.number().optional().nullable().default(null),
  remodeledYear: z.number().optional().nullable().default(null),
  propertyArea: z.number().optional().nullable().default(null),
  interiorArea: z.number().optional().nullable().default(null),
  flooringMaterials: z
    .array(z.nativeEnum(FLOORING_MATERIALS))
    .optional()
    .nullable()
    .default([]),
  acTypes: z.array(z.nativeEnum(AC_TYPES)).optional().nullable().default([]),
  highlights: z.array(z.string().trim().min(1)).optional().nullable().default([]),
  parking: PropertyParkingSchema.optional().nullable().default(null),
  appliances: z.array(z.nativeEnum(APPLIANCES)).optional().nullable().default([]),
  neighborhoodDetails: z
    .object({
      nearbySchools: z.array(z.string().trim().min(1)).optional(),
      nearbyHospitals: z.array(z.string().trim().min(1)).optional(),
      nearbyRetails: z.array(z.string().trim().min(1)).optional(),
    })
    .optional()
    .nullable()
    .default(null),
  hoaInformation: z.string().trim().optional().nullable().default(null),
  otherInformation: z.string().trim().optional().nullable().default(null),
});

// export const PropertyPatchSchema = PropertySchema.partial().merge(z.object({
//   location: PropertyLocationSchema.partial()
// }));

export const PropertyPatchSchema = PropertySchema.deepPartial();

export const PropertyFilterSchema = z.object({
  isActive: z.boolean().optional(),
  isPopular: z.boolean().optional(),
  fullBedCount: z.number().optional(),
  fullBathCount: z.number().optional(),
  halfBedCount: z.number().optional(),
  halfBathCount: z.number().optional(),
  type: z.nativeEnum(PROPERTY_TYPES).optional(),
  types: z.array(z.nativeEnum(PROPERTY_TYPES)).optional(),
  priceRange: PropertyPriceRange.optional(),
  sort: FilterSortSchema.merge(
    z.object({
      field: z.nativeEnum(PROPERTY_SORTS),
    }),
  ).optional(),
});
