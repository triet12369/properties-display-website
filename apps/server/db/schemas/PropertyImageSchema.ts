import { ObjectId } from 'mongodb';
import { z } from 'zod';

export const PropertyImageSchema = z.object({
  _id: z.instanceof(ObjectId),
  propertyId: z.string(),
  mimeType: z.string(),
  width: z.number(),
  height: z.number(),
});

export const PropertyImageFilterSchema = z.object({
  propertyId: z.string(),
});

export const PropertyImageUploadRequestSchema = z.object({
  propertyId: z.number(),
});
