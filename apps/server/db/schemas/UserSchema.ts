import { ContactSchema } from './ContactSchema';
import { z } from 'zod';

export const UserSchema = z.object({
  userId: z.string(),
  contact: ContactSchema.nullable()
});

export const LoginFormSchema = z.object({
  // Google OAuth login
  googleOAuth: z.object({
    idToken: z.string(),
  }),
  basicAuth: z.object({
    username: z.string(),
    password: z.string(),
  })
})
  .partial();
