import { z } from 'zod';

export const JWTSchema = z.object({
  access_token: z.string(),
  expire_at: z.number(),
  type: z.string(),
});