import { UserSchema, LoginFormSchema } from './../db/schemas/UserSchema';
import {
  PropertyImageFilterSchema,
  PropertyImageSchema,
  PropertyImageUploadRequestSchema,
} from './../db/schemas/PropertyImageSchema';
import { PropertyFilterSchema } from './../db/schemas/PropertySchema';
import { z } from 'zod';
import {
  ContactSchema,
  PropertyLocationSchema,
  PropertyParkingSchema,
  PropertyPricingSchema,
  PropertySchema,
} from '../db/schemas';
import { JWTSchema } from '../db/schemas/JWT';

export type Property = z.infer<typeof PropertySchema>;

export type PropertyPricing = z.infer<typeof PropertyPricingSchema>;

export type PropertyLocation = z.infer<typeof PropertyLocationSchema>;

export type PropertyParking = z.infer<typeof PropertyParkingSchema>;

export type Contact = z.infer<typeof ContactSchema>;

export type PropertyFilter = z.infer<typeof PropertyFilterSchema>;

export type PropertyImage = z.infer<typeof PropertyImageSchema>;

export type PropertyImageFilter = z.infer<typeof PropertyImageFilterSchema>;

export type PropertyImageUploadRequest = z.infer<typeof PropertyImageUploadRequestSchema>;

export type User = z.infer<typeof UserSchema>;

export type LoginForm = z.infer<typeof LoginFormSchema>;

export type JWT = z.infer<typeof JWTSchema>;
