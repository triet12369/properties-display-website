import { RENTAL_DURATION_DIVIDERS, US_STATES } from '../constants';
import { NextFunction, Request, Response } from 'express';

export type NeighborhoodDetails = {
  [key: string]: string;
};

export type RentalDurationDivider = typeof RENTAL_DURATION_DIVIDERS[number];

export type USState = typeof US_STATES[number];

export type RouteFunction = (req: Request, res: Response, next: NextFunction) => any;

export type WithMongoId<T> = {
  _id: string;
} & T;

export * from './schemaTypes';
