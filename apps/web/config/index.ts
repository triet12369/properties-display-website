export const COMPANY_NAME = 'CornHusker Homes';

export const FOOTER_TEXT = 'From Nebraska with ❤️';

export const DEFAULT_CONTACT = {
  ADDRESS: 'Lincoln, Nebraska',
  PHONE: '123-456-7890',
};

export const DEFAULT_STATE = 'NE' as const;

export const HEADER_HEIGHT = 60;
export const FOOTER_HEIGHT = 60;

export const APP_PREFIX = 'pdw_app';
export const BASE_URL =
  (typeof window !== 'undefined' && window.origin) || (process.env.BASE_URL as string);

export * from './propertyFormDefaults';
export * from './apiEndpoints';
