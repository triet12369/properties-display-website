import { APPLIANCES } from '../../server/constants';

export const DEFAULT_APPLIANCES: APPLIANCES[] = [
  APPLIANCES.Dishwasher,
  APPLIANCES.Freezer,
  APPLIANCES.Refrigerator,
  APPLIANCES.WaterHeater,
  APPLIANCES.Dryer,
  APPLIANCES.WashingMachine,
  APPLIANCES.Dishwasher,
  APPLIANCES.Oven,
];
