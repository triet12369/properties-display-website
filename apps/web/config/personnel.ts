import tamPhoto from '../assets/images/founder2.jpg';
import { Person } from '../types/Person';

const PERSONNEL: Person[] = [
  {
    name: 'Tam Nguyen',
    description: 'Founder',
    email: 'tam.nguyen@cornhuskerhomes.com',
    imageSrc: tamPhoto,
  },
];

export default PERSONNEL;
