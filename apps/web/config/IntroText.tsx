import { createStyles, Text } from '@mantine/core';
import React, { ReactNode } from 'react';

const useStyles = createStyles((theme) => ({
  description: {
    color: theme.white,
    opacity: 0.75,
    maxWidth: 500,

    [theme.fn.smallerThan('md')]: {
      maxWidth: '100%',
    },
  },
}));

const BigText = (props: { children: ReactNode }) => {
  const { children } = props;
  return (
    <Text
      component="span"
      weight={900}
      //   color="green"
      //   variant="gradient"
      //   gradient={{ from: theme.primaryColor, to: 'secondary' }}
    >
      {children}
    </Text>
  );
};

const IntroText = () => {
  const { classes } = useStyles();

  return (
    <Text className={classes.description} mt={30}>
      The CornHusker Homes Real Estate Company is one of the most{' '}
      <BigText>innovative, creative and forward-thinking</BigText> real estate companies
      in Lincoln, Nebraska. We make your life <BigText>easy</BigText>.
      <Text>
        We help you buy, sale, rent, trade in your houses and{' '}
        <BigText>buy back if you are not satisfied 100% with your purchase here.</BigText>
      </Text>
    </Text>
  );
};

export default IntroText;
