import { MyMantineThemeOverride } from '../types/MyMantineThemeOverride';

//common across

const MY_DEFAULT_THEME: MyMantineThemeOverride = {
  fontFamily: 'Roboto, sans-serif',
  primaryColor: 'blue',
  loader: 'dots',
  other: { secondaryColor: 'red' },
};

export default MY_DEFAULT_THEME;
