import { PropertyImage } from '../../../server/types';

export type IPropertyImageDTO = PropertyImage;
