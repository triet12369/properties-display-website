import { WithMongoId, Property } from '../../../server/types';
// import { PartialDeep } from 'type-fest';

export type IPropertyDTO = WithMongoId<Property>;

export type IPropertyForm = Partial<Property>;
