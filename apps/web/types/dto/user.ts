import { User } from 'next-auth';

import { IJWT } from './../index';

export type WithJWT<T> = { jwt: IJWT } & T;
export type IAuthUser = WithJWT<User>;
