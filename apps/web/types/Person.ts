import { StaticImageData } from 'next/image';

export type Person = {
  name: string;
  description?: string;
  email?: string;
  imageSrc: string | StaticImageData;
};
