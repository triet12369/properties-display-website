import { DefaultSession } from 'next-auth';

import { JWT } from '../../server/types';

declare module 'next-auth' {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    user: {
      /** Jwt Token object returned by our backend */
      jwt: JWT;
    } & DefaultSession['user'];
  }
}
