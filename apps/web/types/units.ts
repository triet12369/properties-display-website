import { RentalDurationDivider } from './../../server/types';
/**
 * Handles conversion of unit type to readable strings
 */

/**
 * Rental Duration
 */

export const RENTAL_DURATION_UNIT: Record<RentalDurationDivider, string> = {
  perMonth: 'month',
  per6Months: '6 months',
  perYear: 'year',
};
