import { UrlObject } from 'url';

import { PROPERTY_TYPES } from '../../server/constants';
import { PropertyFilter, LoginForm, JWT } from '../../server/types';

import { PropertyImageFilter } from './../../server/types/schemaTypes';

export type RouteItem = {
  label: string;
  href: UrlObject & {
    query?: {
      type?: IPropertyType;
    };
  };
};

export type IPropertyFilter = PropertyFilter;

export type IPropertyImageFilter = PropertyImageFilter;

export type IPropertyType = PROPERTY_TYPES;

export type ILoginForm = LoginForm;

export type IJWT = JWT;

export * from './dto';
