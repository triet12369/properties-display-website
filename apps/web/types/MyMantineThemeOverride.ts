import { MantineColor, MantineThemeOverride } from '@mantine/core';

declare module '@mantine/core' {
  export interface MantineThemeOther {
    secondaryColor: MantineColor;
  }
}

export type MyMantineThemeOverride = MantineThemeOverride;
