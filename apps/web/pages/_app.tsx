import '../styles/globals.css';
import { ColorScheme, ColorSchemeProvider, MantineProvider } from '@mantine/core';
import { ModalsProvider } from '@mantine/modals';
import { NotificationsProvider } from '@mantine/notifications';
import { Session } from 'next-auth';
import { SessionProvider } from 'next-auth/react';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { useCallback, useState } from 'react';
import { QueryClientProvider } from 'react-query';

import DEFAULT_THEME from '../config/theme';
import { queryClient } from '../lib';

type MyAppProps = AppProps & {
  colorScheme: ColorScheme;
  pageProps: {
    session: Session;
  };
};

function MyApp(props: MyAppProps) {
  const { Component, pageProps, colorScheme: colorSchemeFromProps } = props;
  const { session, ...filteredPageProps } = pageProps;
  const [colorScheme, setColorScheme] = useState<ColorScheme>(colorSchemeFromProps); // TODO: use localStorage to store colorScheme

  const toggleColorScheme = useCallback(
    (value?: ColorScheme) =>
      setColorScheme((oldScheme: ColorScheme) => {
        return value || oldScheme === 'dark' ? 'light' : 'dark';
      }),
    [],
  );

  return (
    <>
      <Head>
        <title>{process.env.APP_TITLE}</title>
      </Head>
      <SessionProvider session={session}>
        <ColorSchemeProvider
          colorScheme={colorScheme}
          toggleColorScheme={toggleColorScheme}
        >
          <MantineProvider
            withGlobalStyles
            withNormalizeCSS
            theme={{
              /** Put your mantine theme override here */
              colorScheme,
              ...DEFAULT_THEME,
            }}
          >
            <ModalsProvider>
              <NotificationsProvider>
                <QueryClientProvider client={queryClient}>
                  <Component {...filteredPageProps} />;
                </QueryClientProvider>
              </NotificationsProvider>
            </ModalsProvider>
          </MantineProvider>
        </ColorSchemeProvider>
      </SessionProvider>
    </>
  );
}

export default MyApp;
