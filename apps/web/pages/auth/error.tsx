import { Container, createStyles, Title, Text, Button, Group, Box } from '@mantine/core';
import type { NextPage } from 'next';
import Link from 'next/link';

import Layout from '../../components/Layout';
import { ROUTES } from '../../routes';
import useGlobalStyles from '../../styles/globalStyles';
import usePageContentStyles from '../../styles/pageContentStyles';

const useStyles = createStyles((theme) => ({
  root: {
    paddingTop: 80,
    paddingBottom: 120,
    backgroundColor: theme.fn.variant({ variant: 'filled', color: theme.primaryColor })
      .background,
  },
  darkenBackground: {
    opacity: '50%',
    backgroundColor: 'black',
  },
  title: {
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    textAlign: 'center',
    fontWeight: 900,
    fontSize: 38,
    color: theme.colors.red[4],

    [theme.fn.smallerThan('sm')]: {
      fontSize: 32,
    },
  },
  description: {
    maxWidth: 540,
    margin: 'auto',
    marginTop: theme.spacing.xl,
    marginBottom: theme.spacing.xl * 1.5,
    color: theme.colors[theme.primaryColor][1],
  },
  wrapper: {
    position: 'relative',
    backgroundColor: 'black',
  },
  container: {
    position: 'absolute',
  },
  background: {
    opacity: '30%',
  },
}));

const Property: NextPage = () => {
  const { classes: pageClasses } = usePageContentStyles();
  const { classes: globalClasses, cx } = useGlobalStyles();
  const { classes } = useStyles();

  return (
    <Layout>
      <div
        className={cx(
          globalClasses.minContentHeight,
          globalClasses.centerContent,
          classes.wrapper,
        )}
      >
        <Box className={cx(pageClasses.background, classes.background)} />
        <Container className={classes.container}>
          <Title className={classes.title}>Authentication error</Title>
          <Group position="center">
            <Button variant="outline" size="md">
              <Link href={ROUTES.HOME.href}>Return to home</Link>
            </Button>
          </Group>
        </Container>
      </div>
    </Layout>
  );
};

export default Property;
