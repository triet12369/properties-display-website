import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React from 'react';

const GoogleAuthCallback: NextPage = () => {
  const router = useRouter();
  console.log('router', router);
  return <div>GoogleAuthCallback;</div>;
};

export default GoogleAuthCallback;
