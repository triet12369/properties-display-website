import { createStyles, Grid, Stack } from '@mantine/core';
import type { NextPage } from 'next';

import AboutHeading from '../../components/About/AboutHeading';
import AboutOwner from '../../components/About/AboutOwner';
import AboutText from '../../components/About/AboutText';
import Layout from '../../components/Layout';
import usePageContentStyles from '../../styles/pageContentStyles';

const useStyles = createStyles((theme) => ({
  centerizeChildren: {
    display: 'flex',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formatText: {},
  //add padding object
}));

const About: NextPage = () => {
  const { classes: pageClasses, cx } = usePageContentStyles();
  const { classes } = useStyles();
  return (
    <Layout>
      <div className={cx(pageClasses.wrapper, classes.centerizeChildren)}>
        <Grid grow m={50} justify="center">
          <Grid.Col className={classes.centerizeChildren}>
            <Stack>
              <AboutHeading />
              <AboutText />
            </Stack>
          </Grid.Col>
          <Grid.Col className={classes.centerizeChildren}>
            <AboutOwner />
          </Grid.Col>
        </Grid>
      </div>
    </Layout>
  );
};

export default About;
