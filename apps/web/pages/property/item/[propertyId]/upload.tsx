import { Divider, Stack, Text } from '@mantine/core';
import { openConfirmModal } from '@mantine/modals';
import type { GetServerSideProps, NextPage } from 'next';
import { useSession } from 'next-auth/react';
import { MyPageTitle } from 'ui';

import { useDeleteImage } from '../../../../api/deletePropertyImage';
import { getManyPropertyImages } from '../../../../api/getPropertyImages';
import { useUploadImages } from '../../../../api/uploadImages';
import { ImageManagementGrid } from '../../../../components/Image/ImageManagementGrid';
import Layout from '../../../../components/Layout';
import { ProtectedRoute } from '../../../../components/ProtectedRoute';
import usePageContentStyles from '../../../../styles/pageContentStyles';
import { IPropertyImageDTO } from '../../../../types/dto/propertyImage';

type PropertyImageUploadProps = {
  propertyImages: IPropertyImageDTO[];
  propertyId: string;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { params } = context;
  const propertyId = params?.propertyId as string;
  let propertyImages;
  if (propertyId) propertyImages = await getManyPropertyImages({ propertyId });
  return { props: { propertyImages, propertyId } };
};

const PropertyImageUpload: NextPage<PropertyImageUploadProps> = (props) => {
  const { propertyImages, propertyId } = props;
  const { mutate: uploadImages } = useUploadImages({});
  const { mutate: deleteImage } = useDeleteImage({});
  const { classes: pageClasses } = usePageContentStyles();
  const session = useSession();
  const handleUpload = (files: File[]) => {
    if (files.length) uploadImages({ propertyId, files, jwt: session.data?.user.jwt });
  };
  const handleDelete = (imageId: IPropertyImageDTO['_id']) => {
    openConfirmModal({
      title: 'Confirm image deletion',
      children: <Text size="sm">Are you sure you want to delete this image?</Text>,
      labels: { confirm: 'Confirm', cancel: 'Cancel' },
      confirmProps: { color: 'red' },
      onCancel: () => {},
      onConfirm: () => deleteImage({ imageId, jwt: session.data?.user.jwt }),
    });
  };

  return (
    <Layout>
      <ProtectedRoute>
        <Stack p="xs" justify="center" className={pageClasses.wrapper}>
          <MyPageTitle>Images Management</MyPageTitle>
          <Divider></Divider>
          <ImageManagementGrid
            onDeleteSubmit={handleDelete}
            onUploadSubmit={handleUpload}
            imageIds={propertyImages.map((item) => item._id)}
          />
        </Stack>
      </ProtectedRoute>
    </Layout>
  );
};

export default PropertyImageUpload;
