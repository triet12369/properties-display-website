import { Container } from '@mantine/core';
import type { GetServerSideProps, NextPage } from 'next';

import { getOneProperty } from '../../../../api/getProperties';
import { getManyPropertyImages } from '../../../../api/getPropertyImages';
import Layout from '../../../../components/Layout';
import PropertyPage from '../../../../components/PropertyPage/PropertyPage';
import usePageContentStyles from '../../../../styles/pageContentStyles';
import { IPropertyDTO, IPropertyImageDTO } from '../../../../types/dto';

type PropertyItemProps = {
  property?: IPropertyDTO;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { params } = context;
  const propertyId = params?.propertyId as string;
  if (propertyId) {
    const property = await getOneProperty({ propertyId });
    return {
      props: {
        property,
      },
    };
  }
  return { props: {} };
};

const PropertyItem: NextPage<PropertyItemProps> = (props) => {
  const { property } = props;
  const { classes: pageClasses } = usePageContentStyles();
  return (
    <Layout>
      <Container className={pageClasses.wrapper} py="sm">
        {property && <PropertyPage property={property} />}
      </Container>
    </Layout>
  );
};

export default PropertyItem;
