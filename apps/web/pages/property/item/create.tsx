import { Stack, Divider } from '@mantine/core';
import { showNotification } from '@mantine/notifications';
import type { NextPage } from 'next';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import { MyPageTitle } from 'ui';

import { useCreateProperty } from '../../../api/createProperty';
import Layout from '../../../components/Layout';
import { PropertyForm } from '../../../components/Property/PropertyForm';
import { ProtectedRoute } from '../../../components/ProtectedRoute';
import { ROUTES } from '../../../routes';
import usePageContentStyles from '../../../styles/pageContentStyles';
import { IPropertyForm } from '../../../types/dto';

const Property: NextPage = () => {
  const router = useRouter();
  const sessionData = useSession();
  const { classes: pageClasses } = usePageContentStyles();
  const createProperty = useCreateProperty({
    config: {
      onSuccess: () => {
        showNotification({
          message: 'Property created successfully.',
        });
        router.push(ROUTES.PROPERTY_MANAGEMENT.href);
      },
    },
  });
  const onSubmit = (data: IPropertyForm) => {
    createProperty.mutate({ body: data, jwt: sessionData.data?.user.jwt });
  };
  return (
    <Layout>
      <ProtectedRoute>
        <Stack spacing="xs" p="xs" className={pageClasses.wrapper}>
          <MyPageTitle>Create</MyPageTitle>
          <Divider p="xs"></Divider>
          <PropertyForm onSubmit={onSubmit} />
        </Stack>
      </ProtectedRoute>
    </Layout>
  );
};

export default Property;
