import { createStyles, Container } from '@mantine/core';
import type { GetServerSideProps, NextPage } from 'next';

import { PROPERTY_TYPES } from '../../../server/constants';
import { getManyProperties } from '../../api/getProperties';
import Layout from '../../components/Layout';
import { PropertyCardList } from '../../components/Property/PropertyCardList';
import usePageContentStyles from '../../styles/pageContentStyles';
import { IPropertyDTO, IPropertyFilter } from '../../types';

export const getServerSideProps: GetServerSideProps<PropertyListProps> = async (
  context,
) => {
  const { query } = context;
  const { type } = query as IPropertyFilter;
  // console.log('type', type);
  let data: IPropertyDTO[];
  switch (type) {
    case PROPERTY_TYPES.Sale:
    case PROPERTY_TYPES.Rental:
      data = await getManyProperties({
        isActive: true,
        types: [type, PROPERTY_TYPES.Both],
      });
      break;
    case PROPERTY_TYPES.Both:
      data = await getManyProperties({
        isActive: true,
        type,
      });
      break;
    default:
      data = [];
      break;
  }

  return { props: { properties: data } };
};

const useStyles = createStyles((theme) => ({
  wrapper: {
    display: 'flex',
    width: '100%',
    height: '100%',
    alignItems: 'start',
  },
}));

type PropertyListProps = {
  properties: IPropertyDTO[];
};

const PropertyList: NextPage<PropertyListProps> = (props) => {
  const { classes: pageClasses } = usePageContentStyles();
  const { classes, cx } = useStyles();
  const { properties } = props;

  return (
    <Layout>
      <Container className={cx(pageClasses.wrapper, classes.wrapper)}>
        <PropertyCardList data={properties} />
      </Container>
    </Layout>
  );
};

export default PropertyList;
