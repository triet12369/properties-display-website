import { Box, Divider, Stack } from '@mantine/core';
import type { NextPage } from 'next';
import Link from 'next/link';
import { MyButton, MyPageTitle, MyTooltip } from 'ui';

import PropertyAdminList from '../../components/Admin/PropertyAdminList';
import Layout from '../../components/Layout';
import { ProtectedRoute } from '../../components/ProtectedRoute';
import { ROUTES } from '../../routes';
import usePageContentStyles from '../../styles/pageContentStyles';

const PropertyManagement: NextPage = () => {
  const { classes: pageClasses } = usePageContentStyles();
  return (
    <Layout>
      <ProtectedRoute>
        <Stack p="xs" className={pageClasses.wrapper}>
          <MyPageTitle>Property List</MyPageTitle>
          <Divider />
          <Box>
            <Link href={ROUTES.PROPERTY_CREATE.href}>
              <MyTooltip label="Add a new property">
                <MyButton
                  variant="filled"
                  color="green.8"
                  sx={{ textAlign: 'center' }}
                  size="xs"
                >
                  New
                </MyButton>
              </MyTooltip>
            </Link>
          </Box>

          <PropertyAdminList />
        </Stack>
      </ProtectedRoute>
    </Layout>
  );
};

export default PropertyManagement;
