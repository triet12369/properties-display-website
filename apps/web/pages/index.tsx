import { Transition, Box } from '@mantine/core';
import { useScrollIntoView } from '@mantine/hooks';
import type { NextPage } from 'next';
import { RefObject, useEffect, useState } from 'react';
import { useInView } from 'react-intersection-observer';
import { AnimatedDownIcon } from 'ui';

import { Introduction } from '../components/Home/Introduction';
import Layout from '../components/Layout';
import { PopularProperties } from '../components/Property/PopularProperties';
import useGlobalStyles from '../styles/globalStyles';
import useStyles from '../styles/home.styles';
import usePageContentStyles from '../styles/pageContentStyles';

const TRANSITION_DURATION = 1000;

const Home: NextPage = () => {
  const { classes: pageClasses } = usePageContentStyles();
  const { classes, cx } = useStyles();
  const { classes: globalClasses } = useGlobalStyles();
  const [mounted, setMounted] = useState<boolean>(false);
  const [downIconMounted, setDownIconMounted] = useState<boolean>(false);
  const { targetRef, scrollIntoView } = useScrollIntoView();
  const { ref, inView: propertiesListInView } = useInView({
    threshold: 0.5,
  });

  useEffect(() => {
    // run animation on client
    setMounted(true);
    setTimeout(() => setDownIconMounted(true), TRANSITION_DURATION / 2);
  }, []);

  return (
    <Layout>
      <div className={pageClasses.wrapper}>
        <div className={pageClasses.background}>
          <div className={pageClasses.linearGradient}></div>
          <div className={classes.welcomeContent}>
            <Transition
              transition="pop-top-right"
              duration={TRANSITION_DURATION}
              mounted={mounted}
            >
              {(styles) => (
                <>
                  <div className={classes.contentWrapper} style={styles}>
                    <Introduction />
                  </div>
                </>
              )}
            </Transition>
            <Transition
              duration={TRANSITION_DURATION}
              mounted={downIconMounted && !propertiesListInView}
              transition="fade"
            >
              {(styles) => (
                <Box
                  mb="md"
                  className={cx(
                    classes.downIcon,
                    classes.contentWrapper,
                    globalClasses.centerContentSmallerThanMd,
                  )}
                  style={styles}
                >
                  <AnimatedDownIcon onClick={(e) => scrollIntoView()} />
                </Box>
              )}
            </Transition>
          </div>
          <Box
            ref={targetRef as RefObject<HTMLDivElement>}
            className={classes.contentWrapper}
          >
            <div ref={ref}>
              <PopularProperties />
            </div>
          </Box>
        </div>
      </div>
    </Layout>
  );
};

export default Home;
