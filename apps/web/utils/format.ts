import { IPropertyDTO } from './../types/dto/property';
export const getFullAddressStr = (location: IPropertyDTO['location']) =>
  `${location.address}, ${location.city}, ${location.state} ${location.zip}`;
