import { createStyles, Group, Text, Title, useMantineTheme } from '@mantine/core';
import Link from 'next/link';
import { MyButton } from 'ui';

import { COMPANY_NAME } from '../../config';
import IntroText from '../../config/IntroText';
import { ROUTES } from '../../routes';
import useGlobalStyles from '../../styles/globalStyles';

const useStyles = createStyles((theme) => ({
  companyText: {
    color: theme.white,
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    fontWeight: 900,
    lineHeight: 1.05,
    maxWidth: 500,
    fontSize: 48,
  },
  buttonGroup: {},
}));

export const Introduction = () => {
  const theme = useMantineTheme();
  const { classes, cx } = useStyles();
  const { classes: globalClasses } = useGlobalStyles();
  return (
    <>
      <Title className={classes.companyText}>
        Welcome to{' '}
        <Text
          component="span"
          inherit
          variant="gradient"
          gradient={{
            from: theme.primaryColor,
            to: theme.other.secondaryColor,
          }}
        >
          {COMPANY_NAME}
        </Text>{' '}
      </Title>
      <IntroText />
      <Group
        position="left"
        spacing="sm"
        mt={30}
        className={cx(classes.buttonGroup, globalClasses.centerContentSmallerThanMd)}
      >
        <Link
          key={ROUTES.PROPERTY_LIST_BUY.label}
          href={ROUTES.PROPERTY_LIST_BUY.href}
          passHref
        >
          <MyButton size="md" variant="outline">
            {ROUTES.PROPERTY_LIST_BUY.label}
          </MyButton>
        </Link>
        <Link
          key={ROUTES.PROPERTY_LIST_RENT.label}
          href={ROUTES.PROPERTY_LIST_RENT.href}
          passHref
        >
          <MyButton size="md" variant="outline" color={theme.other.secondaryColor}>
            {ROUTES.PROPERTY_LIST_RENT.label}
          </MyButton>
        </Link>
      </Group>
    </>
  );
};
