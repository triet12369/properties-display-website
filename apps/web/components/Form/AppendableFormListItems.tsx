import { Stack, Text, Box, ActionIcon } from '@mantine/core';
import { PlusIcon } from '@modulz/radix-icons';
import React, { FC, ReactNode } from 'react';
import {
  DragDropContext,
  Droppable,
  resetServerContext,
  DragDropContextProps,
  Direction,
} from 'react-beautiful-dnd';

type CommonProps = {
  onAddField: () => void;
  children: ReactNode;
  title?: string;
};
type AppendableFormListItemsProps = CommonProps &
  (
    | {
        draggable: false;
      }
    | {
        draggable: true;
        DragDropContextProps?: Omit<DragDropContextProps, 'children' | 'onDragEnd'>;
        onDragEnd: DragDropContextProps['onDragEnd'];
        droppableId?: string;
        droppableDirection?: Direction;
      }
  );

export const AppendableFormListItems: FC<AppendableFormListItemsProps> = (props) => {
  const { onAddField, title, children } = props;

  if ('draggable' in props && props.draggable) {
    const {
      onDragEnd,
      droppableId = 'dnd-list',
      droppableDirection = 'vertical',
      title,
      children,
    } = props;
    resetServerContext();
    return (
      <DragDropContext {...props.DragDropContextProps} onDragEnd={onDragEnd}>
        <Droppable droppableId={droppableId} direction={droppableDirection}>
          {(provided) => (
            <Stack
              {...provided.droppableProps}
              ref={provided.innerRef}
              spacing={0}
              align="center"
              sx={{ border: '1px dashed grey', borderRadius: 3 }}
              p="xs"
            >
              {title && <Text weight={500}>{title}</Text>}
              {children}
              {provided.placeholder}
              <Box>
                <ActionIcon variant="outline" color="green" onClick={onAddField}>
                  <PlusIcon />
                </ActionIcon>
              </Box>
            </Stack>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
  return (
    <Box>
      <Stack
        spacing={0}
        align="center"
        sx={{ border: '1px dashed grey', borderRadius: 3 }}
        p="xs"
      >
        {title && <Text weight={500}>{title}</Text>}
        {children}
        <Box>
          <ActionIcon variant="outline" color="green" onClick={onAddField}>
            <PlusIcon />
          </ActionIcon>
        </Box>
      </Stack>
    </Box>
  );
};
