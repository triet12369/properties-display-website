import { createStyles, Table, CSSObject, ActionIcon, Center, Text } from '@mantine/core';
import { openConfirmModal } from '@mantine/modals';
import { NextLink } from '@mantine/next';
import {
  CheckIcon,
  Cross1Icon,
  ImageIcon,
  Pencil2Icon,
  TrashIcon,
} from '@modulz/radix-icons';
import { useSession } from 'next-auth/react';
import Link from 'next/link';
import React, { FC } from 'react';
import { MyTooltip } from 'ui/Buttons';

import { useDeleteProperty } from '../../api/deleteProperty';
import { useSearchProperty } from '../../api/getProperties';
import { ROUTES } from '../../routes';
import useGlobalStyles from '../../styles/globalStyles';
import { getFullAddressStr } from '../../utils/format';

const useStyles = createStyles((_theme) => ({
  tableWrapper: {
    overflowX: 'auto',
  },
  tableRow: {
    '& > td': {
      margin: 'auto',
    },
  },
  tableCell: {},
  center: {
    textAlign: 'center !important' as CSSObject['textAlign'],
    verticalAlign: 'middle',
  },
  idHeader: {
    width: '10%',
  },
  linkDecoration: {
    textDecoration: 'underline',
    color: 'blue',
  },
}));

const useActionIconsStyles = createStyles((_theme) => ({
  actionIcon: {
    '& > svg': {
      width: '100%',
      height: '100%',
      maxHeight: '25px',
    },
  },
}));

const Actions: FC<{ id: string; address: string }> = (props) => {
  const { classes } = useActionIconsStyles();
  const { classes: globalClasses, cx } = useGlobalStyles();
  const session = useSession();
  const deleteMutation = useDeleteProperty({});

  const handleDelete = () => {
    openConfirmModal({
      title: 'Confirm property deletion',
      children: (
        <Text size="sm">
          Are you sure you want to delete property at{' '}
          <Text weight={600} span>
            {props.address}
          </Text>
          ?
        </Text>
      ),
      labels: { confirm: 'Confirm', cancel: 'Cancel' },
      confirmProps: { color: 'red' },
      onCancel: () => {},
      onConfirm: () =>
        deleteMutation.mutate({ propertyId: props.id, jwt: session.data?.user.jwt }),
    });
  };

  return (
    <Center>
      <NextLink
        href={`${ROUTES.PROPERTY_ITEM.href.pathname}/${props.id}/upload`}
        passHref
      >
        <MyTooltip label="Upload images">
          <ActionIcon
            mx="sm"
            size="md"
            className={cx(classes.actionIcon, globalClasses.hoverHighlight)}
          >
            <ImageIcon color="black" />
          </ActionIcon>
        </MyTooltip>
      </NextLink>
      <NextLink href={`${ROUTES.PROPERTY_ITEM.href.pathname}/${props.id}/edit`} passHref>
        <MyTooltip label="Edit">
          <ActionIcon
            mx="sm"
            size="md"
            className={cx(classes.actionIcon, globalClasses.hoverHighlight)}
          >
            <Pencil2Icon color="black" />
          </ActionIcon>
        </MyTooltip>
      </NextLink>

      <MyTooltip label="Delete">
        <ActionIcon
          mx="sm"
          size="md"
          className={cx(classes.actionIcon, globalClasses.hoverHighlight)}
          onClick={handleDelete}
        >
          <TrashIcon color="red" />
        </ActionIcon>
      </MyTooltip>
    </Center>
  );
};

const PropertyAdminList = () => {
  const { classes } = useStyles();
  const { classes: iconClasses, cx } = useActionIconsStyles();

  const { data } = useSearchProperty({
    filter: {},
  });
  const tableHeaders = (
    <tr>
      <th className={classes.idHeader}>ID</th>
      <th>Title</th>
      <th>Address</th>
      <th className={classes.center}>Type</th>
      <th className={classes.center}>ZIP Code</th>
      <th className={classes.center}>Active</th>
      <th className={classes.center}>Is Popular</th>
      <th className={classes.center}>Actions</th>
    </tr>
  );

  const rows =
    data &&
    data.map((item) => (
      <tr key={item._id} className={classes.tableRow}>
        <td>{item._id}</td>
        <td>{item.title}</td>
        <td className={classes.linkDecoration}>
          <Link href={`${ROUTES.PROPERTY_ITEM.href.pathname}/${item._id}`}>
            {getFullAddressStr(item.location)}
          </Link>
        </td>
        <td className={classes.center}>{item.type}</td>
        <td className={classes.center}>{item.location.zip}</td>
        <td className={cx(classes.center, iconClasses.actionIcon)}>
          {item.isActive ? <CheckIcon color="green" /> : <Cross1Icon color="red" />}
        </td>
        <td className={cx(classes.center, iconClasses.actionIcon)}>
          {item.isPopular ? <CheckIcon color="green" /> : <Cross1Icon color="red" />}
        </td>
        <td>
          <Actions id={item._id} address={item.location.address} />
        </td>
      </tr>
    ));
  return (
    <div className={classes.tableWrapper}>
      <Table striped highlightOnHover withColumnBorders withBorder fontSize="md">
        <thead>{tableHeaders}</thead>
        <tbody>{rows}</tbody>
      </Table>
    </div>
  );
};

export default PropertyAdminList;
