import { useSession, signIn } from 'next-auth/react';
import React, { FC } from 'react';

import { IAuthUser } from '../../types/dto/user';
import { NavLabel } from '../Navigation/NavLabel';

import UserMenu, { UserMenuSkeleton } from './UserMenu';

type ISignInButtonProps = {
  isDropdown: boolean;
};

const SignInButton: FC<ISignInButtonProps> = (props) => {
  const { isDropdown } = props;
  const { data, status } = useSession();
  const user = data?.user as IAuthUser | undefined;
  // console.log('session data', data);

  if (status === 'authenticated') return <UserMenu user={user} />;

  if (status === 'loading') return <UserMenuSkeleton />;

  return (
    <NavLabel isDropdown={isDropdown} onClick={() => signIn('google')}>
      Sign In
    </NavLabel>
  );
};

export default SignInButton;
