import { Menu, Box, Paper, Skeleton } from '@mantine/core';
import { useClickOutside, useDisclosure } from '@mantine/hooks';
import { GearIcon, ExitIcon } from '@modulz/radix-icons';
import { signOut } from 'next-auth/react';
import Link from 'next/link';
import React, { FC } from 'react';

import { ROUTES } from '../../routes';
import useGlobalStyles from '../../styles/globalStyles';
import { IAuthUser } from '../../types/dto/user';

import { UserButton } from './UserButton';

export const UserMenuSkeleton = () => (
  <Box
    sx={{ width: '150px', height: '100%', display: 'flex', alignItems: 'center' }}
    m="xs"
  >
    <Skeleton height={40} circle />
    <Box
      pl="xs"
      sx={{
        width: '100px',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
      }}
    >
      <Skeleton height={8} my={3} />
      <Skeleton height={8} my={3} />
    </Box>
  </Box>
);

type IUserMenu = {
  user: IAuthUser | undefined;
};
const UserMenu: FC<IUserMenu> = (props) => {
  const { user } = props;
  const { classes, cx } = useGlobalStyles();
  const [isOpen, { open, close }] = useDisclosure(false);

  const ref = useClickOutside(close);

  return (
    <Box
      px="xs"
      className={cx(classes.hoverHighlight, classes.rounded, classes.centerContent)}
    >
      {/* <div> */}
      <Menu
        shadow="md"
        width={220}
        opened={isOpen}
        position="bottom-end"
        withArrow
        transition="scale-y"
        transitionDuration={300}
      >
        <Menu.Target>
          <UserButton
            image={user?.image}
            name={user?.name}
            email={user?.email}
            onClick={() => open()}
          />
        </Menu.Target>
        <Menu.Dropdown>
          <Paper ref={ref}>
            <Link href={ROUTES.PROPERTY_MANAGEMENT.href}>
              <Menu.Item icon={<GearIcon />}>Property management</Menu.Item>
            </Link>
            <Menu.Divider />
            <Menu.Item color="red" icon={<ExitIcon />} onClick={() => signOut()}>
              Log out
            </Menu.Item>
          </Paper>
        </Menu.Dropdown>
      </Menu>
    </Box>
  );
};

export default UserMenu;
