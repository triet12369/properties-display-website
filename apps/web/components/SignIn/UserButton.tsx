import {
  UnstyledButton,
  UnstyledButtonProps,
  Group,
  Avatar,
  Text,
  createStyles,
} from '@mantine/core';
import { ChevronDownIcon } from '@modulz/radix-icons';
import { FC, forwardRef, MouseEventHandler } from 'react';

const useStyles = createStyles((theme) => ({
  user: {
    display: 'block',
    color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,
    borderRadius: '5px',

    // '&:hover': {
    //   backgroundColor:
    //     theme.colorScheme === 'dark'
    //       ? theme.colors.dark[8]
    //       : theme.colors[theme.primaryColor][0],
    //   transition: 'background-color 500ms ease',
    // },
  },
}));

interface IUserButtonProps extends UnstyledButtonProps {
  image?: string | null;
  name?: string | null;
  email?: string | null;
  icon?: React.ReactNode;
  onClick?: MouseEventHandler<HTMLElement>;
  onMouseEnter?: MouseEventHandler<HTMLElement>;
}

export const UserButton = forwardRef<HTMLButtonElement, IUserButtonProps>(
  ({ image, name, email, icon, ...others }, ref) => {
    const { classes } = useStyles();

    return (
      <UnstyledButton className={classes.user} {...others} ref={ref}>
        <Group sx={{ alignItems: 'center' }}>
          <Avatar src={image} radius="xl" />

          <div style={{ flex: 0 }}>
            <Text size="sm" weight={500}>
              {name}
            </Text>

            <Text color="dimmed" size="xs">
              {email}
            </Text>
          </div>

          {icon || <ChevronDownIcon />}
        </Group>
      </UnstyledButton>
    );
  },
);

UserButton.displayName = 'UserButton';
