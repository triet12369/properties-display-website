import { Stack, Text, useMantineTheme, Card } from '@mantine/core';
import React, { FC, ReactNode } from 'react';
import { GiSpikedFence, GiFairyWand } from 'react-icons/gi';
import { IoIosConstruct } from 'react-icons/io';
import { IoSnow } from 'react-icons/io5';
import { RiParkingBoxLine } from 'react-icons/ri';

import { IPropertyDTO } from '../../types';

type Selector = (property: Partial<IPropertyDTO>) => {
  icon: ReactNode;
  label: string;
  value?: ReactNode;
};
const OVERVIEW_FIELDS: Record<string, Selector> = {
  BUILT_YEAR: (p) => ({
    icon: <IoIosConstruct />,
    label: 'Built in',
    value: p.builtYear,
  }),
  REMODEL_YEAR: (p) => ({
    icon: <GiFairyWand />,
    label: 'Remodeled in',
    value: p.remodeledYear,
  }),
  AC_TYPES: (p) => ({
    icon: <IoSnow />,
    label: 'A/C',
    value: p.acTypes?.length && p.acTypes.join(', '),
  }),
  AREA: (p) => ({
    icon: <GiSpikedFence />,
    label: 'Area',
    value:
      (p.interiorArea || p.propertyArea) &&
      [
        `${
          p.interiorArea
            ? `${p.interiorArea.toLocaleString('en-us')} sqft (interior area)`
            : ''
        }`,
        `${
          p.propertyArea
            ? `${p.propertyArea.toLocaleString('en-us')} sqft (property area)`
            : ''
        }`,
      ]
        .filter(Boolean)
        .join(' | '),
  }),
  PARKING: (p) => ({
    icon: <RiParkingBoxLine />,
    label: 'Parking',
    value:
      (p.parking?.coveredSpots || p.parking?.uncoveredSpots) &&
      [
        `${p.parking.coveredSpots ? `${p.parking.coveredSpots} covered` : ''}`,
        `${p.parking.uncoveredSpots ? `${p.parking.uncoveredSpots} uncovered` : ''}`,
        `${p.parking.features.length ? `${p.parking.features.join(', ')}` : ''}`,
      ]
        .filter(Boolean)
        .join(' | '),
  }),
};

type ContactsPanelProps = {
  property: Partial<IPropertyDTO>;
};
export const ContactsPanel: FC<ContactsPanelProps> = (props) => {
  const { property } = props;
  const theme = useMantineTheme();
  return (
    <Stack>
      {property.contacts ? (
        property.contacts.map((contact) => (
          <Stack>
            <ContactCard {...contact} />
          </Stack>
        ))
      ) : (
        <></>
      )}
    </Stack>
  );
};

const ContactCard: FC<{
  address: string | null;
  name: string;
  phoneNumber: string | null;
  email: string | null;
}> = (props) => {
  const { name, address, phoneNumber, email } = props;
  const formattedPhoneNumber =
    phoneNumber && phoneNumber.match(/^(\d{3})(\d{3})(\d{4})$/);
  return (
    <Card shadow="sm">
      <Text weight={500}>{name}</Text>
      {formattedPhoneNumber && (
        <Text
          weight={300}
        >{`(${formattedPhoneNumber[1]}) ${formattedPhoneNumber[2]} ${formattedPhoneNumber[3]}`}</Text>
      )}
      <Text weight={300}>{address}</Text>
      <Text weight={300}>{email}</Text>
    </Card>
  );
};
