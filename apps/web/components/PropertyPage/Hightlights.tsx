import { Group, Badge } from '@mantine/core';
import React, { FC, PropsWithChildren } from 'react';

import { IPropertyDTO } from '../../types';
type HightlightsCarouselProps = Pick<IPropertyDTO, 'highlights'>;

const HighlightItem: FC<PropsWithChildren> = (props) => (
  // <Badge variant="gradient" gradient={{ from: 'blue', to: 'red' }}>
  //   {props.children}
  // </Badge>
  <Badge variant="dot" size="lg">
    {props.children}
  </Badge>
);

export const HightlightsCarousel: FC<HightlightsCarouselProps> = (props) => {
  const { highlights } = props;

  return (
    <Group>
      {highlights?.map((item) => (
        <HighlightItem key={item}>{item}</HighlightItem>
      ))}
    </Group>
  );
};
