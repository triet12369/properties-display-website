import { Stack, Text, Group, ThemeIcon, Title, Center } from '@mantine/core';
import React, { FC, Fragment, ReactNode } from 'react';
import { BsStars, BsInfoSquare } from 'react-icons/bs';
import { FaBath, FaBed, FaToilet, FaTemperatureHigh } from 'react-icons/fa';
import { HiOutlineBuildingOffice2 } from 'react-icons/hi2';
import { MdKitchen } from 'react-icons/md';
import { RiParkingBoxLine, RiParkingLine } from 'react-icons/ri';
import { SiElasticstack } from 'react-icons/si';

import { IPropertyDTO } from '../../types';

type Selector = (property: Partial<IPropertyDTO>) => {
  icon?: ReactNode;
  label?: string;
  value?: ReactNode;
};

type FieldDef = Record<string, Selector>;

const BED_BATH_FIELDS: FieldDef = {
  FULL_BATH: (p) => ({
    icon: <FaBath />,
    label: 'Bathrooms',
    value: p.fullBathCount,
  }),
  HALF_BATH: (p) => ({
    icon: <FaToilet />,
    label: '1/2 Bathrooms',
    value: p.halfBathCount,
  }),
  FULL_BED: (p) => ({
    icon: <FaBed />,
    label: 'Bedrooms',
    value: p.fullBedCount,
  }),
  HALF_BED: (p) => ({
    icon: <FaBed />,
    label: '1/2 Bedrooms',
    value: p.halfBedCount,
  }),
};

const FLOORING_FIELDS: FieldDef = {
  FLOORING: (p) => ({
    icon: <SiElasticstack />,
    label: 'Flooring materials',
    value: p.flooringMaterials?.join(', '),
  }),
};

const APPLIANCES_FIELDS: FieldDef = {
  APPLIANCES: (p) => ({
    icon: <MdKitchen />,
    label: 'Appliances',
    value: p.appliances?.join(', '),
  }),
};

const AC_FIELDS: FieldDef = {
  AC: (p) => ({
    icon: <FaTemperatureHigh />,
    label: 'Air conditioning system',
    value: p.acTypes?.join(', '),
  }),
};

const PARKING_FIELDS: FieldDef = {
  PARKING_COVERED: (p) => ({
    icon: <RiParkingBoxLine />,
    label: 'Covered spots',
    value: p.parking?.coveredSpots,
  }),
  PARKING_UNCOVERED: (p) => ({
    icon: <RiParkingLine />,
    label: 'Uncovered spots',
    value: p.parking?.uncoveredSpots,
  }),
  PARKING_FEATURES: (p) => ({
    icon: <BsStars />,
    label: 'Features',
    value: p.parking?.features.length && p.parking?.features.join(', '),
  }),
};

const HOA_INFO_FIELDS: FieldDef = {
  HOA: (p) => ({
    // icon: <HiOutlineBuildingOffice2 />,
    // label: '',
    value: p.hoaInformation && <Text sx={{ whiteSpace: 'pre' }}>{p.hoaInformation}</Text>,
  }),
};

const OTHER_INFO_FIELDS: FieldDef = {
  OTHER: (p) => ({
    // icon: <BsInfoSquare />,
    // label: '',
    value: p.otherInformation,
  }),
};

type FeaturesPanelProps = {
  property: Partial<IPropertyDTO>;
};
export const FeaturesPanel: FC<FeaturesPanelProps> = (props) => {
  const { property } = props;
  return (
    <Stack>
      <Center>
        <Title order={3}>Interior details</Title>
      </Center>
      <Title order={4}>Bedrooms and bathrooms</Title>
      <FieldListings fields={BED_BATH_FIELDS} property={property} />
      <Title order={4}>Flooring</Title>
      <FieldListings fields={FLOORING_FIELDS} property={property} />
      <Title order={4}>Heating & Cooling</Title>
      <FieldListings fields={AC_FIELDS} property={property} />
      <Title order={4}>Appliances</Title>
      <FieldListings fields={APPLIANCES_FIELDS} property={property} />
      <Center>
        <Title order={3}>Property details</Title>
      </Center>
      <Title order={4}>Parking</Title>
      <FieldListings fields={PARKING_FIELDS} property={property} />
      <Center>
        <Title order={3}>Other details</Title>
      </Center>
      <Title order={4}>HOA information</Title>
      <FieldListings fields={HOA_INFO_FIELDS} property={property} />
      <Title order={4}>Other information</Title>
      <FieldListings fields={OTHER_INFO_FIELDS} property={property} />
    </Stack>
  );
};

const FieldListings: FC<{ fields: FieldDef; property: Partial<IPropertyDTO> }> = (
  props,
) => {
  return (
    <>
      {Object.values(props.fields).map((selector, index) => {
        const item = selector(props.property);
        if (item.value) {
          return (
            <Stack key={index}>
              <Group align="center">
                <Text>
                  {item.icon && (
                    <Text px="sm" pl={0} span>
                      <ThemeIcon>{item.icon}</ThemeIcon>
                    </Text>
                  )}
                  {item.label ? `${item.label}:` : ''} {item.value}
                </Text>
              </Group>
            </Stack>
          );
        }
        return <Fragment key={index}></Fragment>;
      })}
    </>
  );
};
