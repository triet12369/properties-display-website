import {
  Stack,
  Text,
  Divider,
  Group,
  useMantineTheme,
  Box,
  ThemeIcon,
} from '@mantine/core';
import React, { FC, Fragment, ReactNode } from 'react';
import { GiSpikedFence, GiFairyWand } from 'react-icons/gi';
import { IoIosConstruct } from 'react-icons/io';
import { IoSnow } from 'react-icons/io5';
import { RiParkingBoxLine } from 'react-icons/ri';
import { MyTooltip } from 'ui';

import { IPropertyDTO } from '../../types';

type Selector = (property: Partial<IPropertyDTO>) => {
  icon: ReactNode;
  label: string;
  value?: ReactNode;
};
const OVERVIEW_FIELDS: Record<string, Selector> = {
  BUILT_YEAR: (p) => ({
    icon: <IoIosConstruct />,
    label: 'Built in',
    value: p.builtYear,
  }),
  REMODEL_YEAR: (p) => ({
    icon: <GiFairyWand />,
    label: 'Remodeled in',
    value: p.remodeledYear,
  }),
  AC_TYPES: (p) => ({
    icon: <IoSnow />,
    label: 'A/C',
    value: p.acTypes?.length && p.acTypes.join(', '),
  }),
  AREA: (p) => ({
    icon: <GiSpikedFence />,
    label: 'Area',
    value:
      (p.interiorArea || p.propertyArea) &&
      [
        `${
          p.interiorArea
            ? `${p.interiorArea.toLocaleString('en-us')} sqft (interior area)`
            : ''
        }`,
        `${
          p.propertyArea
            ? `${p.propertyArea.toLocaleString('en-us')} sqft (property area)`
            : ''
        }`,
      ]
        .filter(Boolean)
        .join(' | '),
  }),
  PARKING: (p) => ({
    icon: <RiParkingBoxLine />,
    label: 'Parking',
    value:
      (p.parking?.coveredSpots || p.parking?.uncoveredSpots) &&
      [
        `${p.parking.coveredSpots ? `${p.parking.coveredSpots} covered` : ''}`,
        `${p.parking.uncoveredSpots ? `${p.parking.uncoveredSpots} uncovered` : ''}`,
        `${p.parking.features.length ? `${p.parking.features.join(', ')}` : ''}`,
      ]
        .filter(Boolean)
        .join(' | '),
  }),
};

type OverViewPanelProps = {
  property: Partial<IPropertyDTO>;
};
export const OverviewPanel: FC<OverViewPanelProps> = (props) => {
  const { property } = props;
  const theme = useMantineTheme();
  return (
    <Stack>
      <Text weight={300}>{property.description} </Text>
      <Divider />
      {Object.values(OVERVIEW_FIELDS).map((selector, index) => {
        const item = selector(property);
        if (item.value) {
          return (
            <Group key={index}>
              <MyTooltip label={item.label}>
                <ThemeIcon>{item.icon}</ThemeIcon>
              </MyTooltip>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Text color={theme.primaryColor}>
                  <Text weight="semibold" span>
                    {item.value}
                  </Text>
                </Text>
              </Box>
            </Group>
          );
        }
        return <Fragment key={index}></Fragment>;
      })}
    </Stack>
  );
};
