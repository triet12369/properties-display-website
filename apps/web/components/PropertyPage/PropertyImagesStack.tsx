import { LoadingOverlay, Center, createStyles, SimpleGrid } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import Image from 'next/image';
import React, { FC, useState } from 'react';

import { createImageURL, useSearchPropertyImages } from '../../api/getPropertyImages';
import { IPropertyImageDTO } from '../../types';

import { Lightbox } from './Lightbox';

type PropertyImagesStackProps = {
  images: IPropertyImageDTO[];
};

const makeImage = (image: IPropertyImageDTO) => ({
  src: createImageURL(image._id),
  width: image.width || 500,
  height: image.height || 500,
});

const boxShadow =
  '0px 3px 5px -1px rgba(0,0,0,0.2),0px 5px 8px 0px rgba(0,0,0,0.14),0px 1px 14px 0px rgba(0,0,0,0.12)';

const useStyles = createStyles((theme) => ({
  image: {
    '&:hover': {
      transform: 'scale(1.02)',
      boxShadow,
      '&:after': {
        boxShadow,
      },
      zIndex: 10,
    },
    transition: 'all 300ms ease',
  },
}));

export const ConnectedPropertyImagesStack: FC<{ propertyId: string }> = (props) => {
  const { data: propertyImages } = useSearchPropertyImages({
    filter: { propertyId: props.propertyId },
  });
  return propertyImages ? (
    <PropertyImagesStack images={propertyImages} />
  ) : (
    <LoadingOverlay visible />
  );
};

export const PropertyImagesStack: FC<PropertyImagesStackProps> = (props) => {
  const { images } = props;
  const { classes } = useStyles();
  const galleryImages = images.map((image) => makeImage(image));
  const [selectedIndex, setSelectedIndex] = useState<number>(-1);
  const [isOpen, { close, open }] = useDisclosure(false);

  return (
    <>
      <SimpleGrid spacing={3} verticalSpacing={3} cols={2}>
        {galleryImages.map((image, index) => (
          <Center
            key={image.src}
            className={classes.image}
            sx={{
              width: '100%',
              height: '300px',
              cursor: 'pointer',
              borderRadius: 3,
              overflow: 'hidden',
              position: 'relative',
            }}
            onClick={() => {
              setSelectedIndex(index);
              open();
            }}
          >
            <Image src={image.src} layout="fill" alt="house" objectFit="cover"></Image>
          </Center>
        ))}
      </SimpleGrid>

      <Lightbox
        images={galleryImages}
        index={selectedIndex}
        open={isOpen}
        close={close}
      />
    </>
  );
};
