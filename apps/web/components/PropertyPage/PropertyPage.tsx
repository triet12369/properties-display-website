import {
  Card,
  Grid,
  Tabs,
  Stack,
  createStyles,
  Center,
  useMantineTheme,
  Divider,
} from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import React, { FC } from 'react';
import { MyPageTitle } from 'ui';

import { DEFAULT_STATE } from '../../config';
import { MIN_CONTENT_HEIGHT } from '../../styles/pageContentStyles';
import { IPropertyDTO } from '../../types';

import { ContactsPanel } from './ContactsPanel';
import { FeaturesPanel } from './FeaturesPanel';
import { OverviewPanel } from './OverviewPanel';
import { ConnectedPropertyImagesStack } from './PropertyImagesStack';
import { PropertyInfo } from './PropertyInfo';

type PropertyPageProps = {
  property: Partial<IPropertyDTO>;
  propertyId?: string;
  // propertyImages: IPropertyImageDTO[];
};

const TABS = {
  OVERVIEW: 'Overview',
  FEATURES: 'Features',
  NEIGHBORHOOD: 'Neighborhood',
  CONTACTS: 'Contacts',
};

const useStyles = createStyles((theme) => ({
  wrapper: {
    display: 'flex',
    width: '100%',
    height: '100%',
    background: theme.white,
    justifyContent: 'center',
    maxWidth: '100%',
    // overflow: 'auto',
  },
  tabLabel: {
    fontWeight: 500,
    color: theme.colors[theme.primaryColor],
  },
}));

const DEFAULT_LOCATION = { address: '', city: '', zip: 0, state: DEFAULT_STATE } as const;
export const PropertyPage: FC<PropertyPageProps> = (props) => {
  const { property, propertyId } = props;
  const { classes } = useStyles();
  const theme = useMantineTheme();
  const isMobile = useMediaQuery(`(max-width: ${theme.breakpoints.sm}px)`, true, {
    getInitialValueInEffect: false,
  });
  // input "property" might be from a Form, which doesnt have an ID yet
  const finalPropertyId = property._id || propertyId;
  const colSpan = isMobile ? 12 : 6;
  return (
    <Card shadow="sm" p={0} className={classes.wrapper}>
      <Grid
        py="xs"
        sx={{ width: '100%', maxHeight: !isMobile ? MIN_CONTENT_HEIGHT : '' }}
      >
        <Grid.Col
          span={colSpan}
          sx={{
            position: 'relative',
            width: '100%',
            maxHeight: '100%',
          }}
        >
          <Stack sx={{ height: '100%' }}>
            {property.title && (
              <Center>
                <MyPageTitle>{property.title}</MyPageTitle>
              </Center>
            )}
            <PropertyInfo
              propertyArea={property.propertyArea || 0}
              pricing={property.pricing || {}}
              highlights={property.highlights || []}
              fullBathCount={property.fullBathCount || 0}
              fullBedCount={property.fullBedCount || 0}
              location={property.location || DEFAULT_LOCATION}
            />
            <Divider />
            <Tabs defaultValue={TABS.OVERVIEW} sx={{ height: '100%', overflowY: 'auto' }}>
              <Tabs.List
                grow
                sx={{ position: 'sticky', top: 0, zIndex: 10, background: theme.white }}
              >
                {Object.entries(TABS).map(([key, val]) => (
                  <Tabs.Tab key={key} value={val} className={classes.tabLabel}>
                    {val}
                  </Tabs.Tab>
                ))}
              </Tabs.List>

              <Tabs.Panel value={TABS.OVERVIEW} pt="xs">
                <OverviewPanel property={property} />
              </Tabs.Panel>
              <Tabs.Panel value={TABS.FEATURES} pt="xs">
                <FeaturesPanel property={property} />
              </Tabs.Panel>
              <Tabs.Panel value={TABS.NEIGHBORHOOD} pt="xs">
                Coming soon!
              </Tabs.Panel>
              <Tabs.Panel value={TABS.CONTACTS} pt="xs">
                <ContactsPanel property={property} />
              </Tabs.Panel>
            </Tabs>
          </Stack>
        </Grid.Col>
        <Grid.Col
          span={colSpan}
          sx={{
            overflow: 'auto',
            width: '100%',
            maxHeight: '100vh',
            position: 'relative',
            background: theme.colors.gray[2],
          }}
        >
          {finalPropertyId && (
            <ConnectedPropertyImagesStack propertyId={finalPropertyId} />
          )}
        </Grid.Col>
      </Grid>
    </Card>
  );
};

export default PropertyPage;
