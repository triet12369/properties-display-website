/* eslint-disable @next/next/no-img-element */
import { Carousel } from '@mantine/carousel';
import { Portal, Center, CloseButton, Transition } from '@mantine/core';
import NextImage from 'next/image';
import React, { FC, MouseEvent } from 'react';

export type IDisplayImage = { src: string; width: number; height: number };
type LightboxProps = {
  images: IDisplayImage[];
  index: number;
  open: boolean;
  close: () => void;
};

export const Lightbox: FC<LightboxProps> = (props) => {
  const { close, images, index, open } = props;
  return (
    <Portal>
      <Transition mounted={open} transition="fade">
        {(styles) => (
          <Center
            style={styles}
            sx={{
              width: '100%',
              height: '100%',
              maxWidth: '100%',
              maxHeight: '100%',
              position: 'fixed',
              top: 0,
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
              backdropFilter: 'blur(5px)',
              zIndex: 201,
            }}
          >
            <CloseButton
              sx={{ position: 'absolute', top: 20, right: 20, zIndex: 101 }}
              size="lg"
              color="blue"
              variant="filled"
              onClick={close}
            />
            <Carousel
              initialSlide={index}
              //   slideSize="50%"
              align="center"
              slideGap="md"
              slidesToScroll={1}
              withControls
              withIndicators
              controlSize={40}
              sx={{
                zIndex: 100,
                maxWidth: '100%',
                height: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              {images.map((image) => (
                <Carousel.Slide
                  key={image.src}
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Center
                    style={styles}
                    sx={{
                      width: image.width,
                      height: image.height,
                      maxHeight: '80vh',
                      maxWidth: '80vw',
                      position: 'relative',
                    }}
                    onClick={(e: MouseEvent<HTMLElement>) => {
                      e.preventDefault();
                      e.stopPropagation();
                    }}
                  >
                    {/* <Box sx={{ position: 'relative' }}></Box> */}
                    <NextImage
                      src={image.src}
                      alt="house"
                      layout="fill"
                      objectFit="contain"
                    />
                  </Center>
                </Carousel.Slide>
              ))}
            </Carousel>
          </Center>
        )}
      </Transition>
    </Portal>
  );
};
