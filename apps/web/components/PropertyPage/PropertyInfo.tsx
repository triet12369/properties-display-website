import { Stack, Title, useMantineTheme, Group, Text, Divider } from '@mantine/core';
import React, { FC } from 'react';

import { IPropertyDTO } from '../../types';
import { RENTAL_DURATION_UNIT } from '../../types/units';
import { getFullAddressStr } from '../../utils/format';

import { HightlightsCarousel } from './Hightlights';

type PropertyInfoProps = Pick<
  IPropertyDTO,
  | 'fullBathCount'
  | 'fullBedCount'
  | 'pricing'
  | 'propertyArea'
  | 'highlights'
  | 'location'
>;

const PriceTitle = (props: {
  type: 'sale' | 'rental';
  val: number;
  duration?: string;
}) => {
  const theme = useMantineTheme();
  const price = `$${props.val.toLocaleString('en-us')}`;
  switch (props.type) {
    case 'sale':
      return (
        <Title order={3} color={theme.primaryColor}>
          Sales price: {price}
        </Title>
      );
    case 'rental':
      return (
        <Title order={3} color={theme.other.secondaryColor}>
          Rental price: {price}/{props.duration}
        </Title>
      );
  }
};

export const PropertyInfo: FC<PropertyInfoProps> = (props) => {
  const bedBathString = [
    props.fullBedCount && `${props.fullBedCount} bd`,
    props.fullBathCount && `${props.fullBathCount} ba`,
    props.propertyArea && `${props.propertyArea} sqft`,
  ].filter((item) => !!item);
  return (
    <Stack sx={{ width: '100%' }} spacing="xs">
      <Group>
        {props.pricing?.salesPrice && (
          <>
            <PriceTitle type="sale" val={props.pricing.salesPrice} />
          </>
        )}
        {props.pricing?.salesPrice && props.pricing?.rentalPrice && (
          <Divider orientation="vertical" />
        )}
        {props.pricing?.rentalPrice && (
          <>
            <PriceTitle
              type="rental"
              val={props.pricing.rentalPrice}
              duration={
                RENTAL_DURATION_UNIT[props.pricing.rentalDurationDivider || 'perMonth']
              }
            />
          </>
        )}
      </Group>
      <Group>
        {bedBathString.map((item, index) => (
          <>
            <Text weight={500}>{item}</Text>
            {index !== bedBathString.length - 1 && <Divider orientation="vertical" />}
          </>
        ))}
      </Group>
      <Text weight={300}>{getFullAddressStr(props.location)}</Text>
      <HightlightsCarousel highlights={props.highlights} />
    </Stack>
  );
};
