import { createStyles, Divider, DividerProps } from '@mantine/core';
import Link from 'next/link';
import React, { useMemo } from 'react';

import { ROUTES } from '../../routes';
import SignInButton from '../SignIn/SignInButton';

import { NavLabel } from './NavLabel';

type INavBarProps = {
  isDropdown: boolean;
  closeBurger?: () => void;
};

const useNavBarStyles = createStyles((theme, navBarProps: INavBarProps) => ({
  wrapper: {
    display: 'flex',
    flexDirection: navBarProps.isDropdown ? 'column' : 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

function NavBar(props: INavBarProps) {
  const { isDropdown, closeBurger } = props;
  const { classes } = useNavBarStyles(props);

  const dividerProps: DividerProps = useMemo(
    () => ({
      sx: {
        width: isDropdown ? '75%' : '',
      },
      mx: 'xs',
      orientation: isDropdown ? 'horizontal' : 'vertical',
    }),
    [isDropdown],
  );

  return (
    <div className={classes.wrapper}>
      <Link key={ROUTES.HOME.label} href={ROUTES.HOME.href} passHref>
        <NavLabel isDropdown={isDropdown} closeBurger={closeBurger}>
          {ROUTES.HOME.label}
        </NavLabel>
      </Link>
      <Divider {...dividerProps} />
      <Link
        key={ROUTES.PROPERTY_LIST_BUY.label}
        href={ROUTES.PROPERTY_LIST_BUY.href}
        passHref
      >
        <NavLabel isDropdown={isDropdown} closeBurger={closeBurger}>
          {ROUTES.PROPERTY_LIST_BUY.label}
        </NavLabel>
      </Link>
      <Divider {...dividerProps} />
      <Link
        key={ROUTES.PROPERTY_LIST_RENT.label}
        href={ROUTES.PROPERTY_LIST_RENT.href}
        passHref
      >
        <NavLabel isDropdown={isDropdown} closeBurger={closeBurger}>
          {ROUTES.PROPERTY_LIST_RENT.label}
        </NavLabel>
      </Link>
      <Divider {...dividerProps} />
      <Link key={ROUTES.ABOUT.label} href={ROUTES.ABOUT.href} passHref>
        <NavLabel isDropdown={isDropdown} closeBurger={closeBurger}>
          {ROUTES.ABOUT.label}
        </NavLabel>
      </Link>
      <Divider {...dividerProps} />
      <SignInButton isDropdown={isDropdown} />
    </div>
  );
}

export default NavBar;
