import {
  Box,
  Button,
  Group,
  NumberInput,
  Radio,
  Select,
  Stack,
  Switch,
  Textarea,
  TextInput,
  Checkbox,
  Center,
  Text,
  ActionIcon,
  createStyles,
  Modal,
} from '@mantine/core';
import { useForm, zodResolver } from '@mantine/form';
import { useDisclosure } from '@mantine/hooks';
import { NextLink } from '@mantine/next';
import { DragHandleDots2Icon, MinusIcon } from '@modulz/radix-icons';
import Link from 'next/link';
import { FC, useEffect } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { MyButton, MyPageTitle, YearSelection } from 'ui';

import {
  AC_TYPES,
  APPLIANCES,
  FLOORING_MATERIALS,
  HIGHLIGHTS_BASE_CHOICES,
  PARKING_FEATURE_BASE_CHOICES,
  PROPERTY_TYPES,
  RENTAL_DURATION_DIVIDERS,
  US_STATES,
  US_STATES_MAP,
} from '../../../server/constants';
import { PropertySchema } from '../../../server/db/schemas/PropertySchema';
import {
  APP_PREFIX,
  DEFAULT_APPLIANCES,
  DEFAULT_CONTACT,
  DEFAULT_STATE,
} from '../../config';
import { ROUTES } from '../../routes';
import { IPropertyDTO, IPropertyForm } from '../../types/dto';
import { RENTAL_DURATION_UNIT } from '../../types/units';
import { AppendableFormListItems } from '../Form/AppendableFormListItems';
import PropertyPage from '../PropertyPage/PropertyPage';

const getFormCacheKey = (id?: string) => `${APP_PREFIX}.user-form-${id ? id : 'temp'}`;

type PropertyFormProps = {
  onSubmit?: (formData: IPropertyForm) => void;
  property?: IPropertyDTO;
  labelSubmit?: string;
};

const useStyles = createStyles((theme) => ({
  fullWidth: {
    width: '100%',
  },
  halfWidth: {
    width: '70%',
    [theme.fn.smallerThan(theme.breakpoints.md)]: {
      width: '100%',
    },
  },
}));
export const PropertyForm: FC<PropertyFormProps> = (props) => {
  const { onSubmit, property, labelSubmit = 'Submit' } = props;
  const { classes } = useStyles();
  const form = useForm<IPropertyForm>({
    initialValues: {
      isActive: true,
      isPopular: false,
      title: '',
      location: {
        city: '',
        zip: 0,
        address: '',
        state: DEFAULT_STATE,
      },
      type: PROPERTY_TYPES.Sale,
      pricing: {
        salesPrice: undefined,
        rentalDurationDivider: RENTAL_DURATION_DIVIDERS[0],
        rentalPrice: undefined,
      },
      fullBedCount: 0,
      fullBathCount: 0,
      halfBedCount: 0,
      halfBathCount: 0,
      description: '',
      contacts: [
        {
          address: DEFAULT_CONTACT.ADDRESS,
          name: 'Iron Man',
          phoneNumber: DEFAULT_CONTACT.PHONE,
          email: 'ayy',
        },
      ],
      builtYear: undefined,
      remodeledYear: undefined,
      propertyArea: 0,
      interiorArea: 0,
      flooringMaterials: [FLOORING_MATERIALS.Wood],
      acTypes: [AC_TYPES.Central],
      highlights: [HIGHLIGHTS_BASE_CHOICES[0]],
      parking: {
        uncoveredSpots: 0,
        coveredSpots: 0,
        features: [],
      },
      appliances: [...DEFAULT_APPLIANCES],
      neighborhoodDetails: {
        nearbySchools: [],
        nearbyHospitals: [],
        nearbyRetails: [],
      },
      hoaInformation: '',
      otherInformation: '',
    },
    validate: zodResolver(PropertySchema),
    validateInputOnChange: true,
  });
  const [isOpen, { toggle, close }] = useDisclosure(false);

  const handleSubmit = (values: IPropertyForm) => {
    switch (values.type) {
      // discard sale/rental price depending on property type
      case PROPERTY_TYPES.Sale:
        if (values.pricing?.rentalPrice) values.pricing.rentalPrice = 0;
        break;
      case PROPERTY_TYPES.Rental:
        if (values.pricing?.salesPrice) values.pricing.salesPrice = 0;
        break;
    }
    // console.log('form value', values);
    onSubmit && onSubmit(values);
  };

  // store form to local storage
  useEffect(() => {
    const storedValue = window.localStorage.getItem(getFormCacheKey(property?._id));
    if (property) {
      (Object.keys(property) as (keyof IPropertyDTO)[]).forEach((key) => {
        if (property[key] === null) delete property[key];
      });
      // console.log('cleaned', property);
      const propertyWithoutID = { ...property, _id: undefined };
      form.setValues(propertyWithoutID);
    } else if (storedValue) {
      try {
        form.setValues(JSON.parse(storedValue));
      } catch (e) {
        console.log('Failed to parse stored value');
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    window.localStorage.setItem(
      getFormCacheKey(property?._id),
      JSON.stringify(form.values),
    );
  }, [form.values, property?._id]);

  return (
    <form onSubmit={form.onSubmit(handleSubmit)}>
      <Center>
        <Stack align="flex-start" className={classes.halfWidth}>
          <Group>
            <Switch
              label="Active (will display to visitors)"
              {...form.getInputProps('isActive')}
              checked={form.getInputProps('isActive').value}
            />
            <Switch
              label="is Popular (will put on Home page)"
              {...form.getInputProps('isPopular')}
              checked={form.getInputProps('isPopular').value}
            />
          </Group>
          <Box className={classes.fullWidth}>
            <TextInput
              label="Title"
              {...form.getInputProps('title')}
              placeholder="The Lakeview"
            />
          </Box>
          <Group>
            <TextInput
              withAsterisk
              label="Address"
              {...form.getInputProps('location.address')}
            />
            <NumberInput
              withAsterisk
              label="Zip"
              min={0}
              {...form.getInputProps('location.zip')}
            />
            <TextInput
              withAsterisk
              label="City"
              {...form.getInputProps('location.city')}
            />
            <Select
              withAsterisk
              label="State"
              searchable
              nothingFound="Not found"
              {...form.getInputProps('location.state')}
              data={US_STATES.map((item) => ({
                value: item,
                label: US_STATES_MAP[item],
              }))}
            />
          </Group>
          <Group>
            <Group>
              <Radio.Group
                {...form.getInputProps('type')}
                withAsterisk
                label="Property type"
              >
                {Object.values(PROPERTY_TYPES).map((item) => (
                  <Radio value={item} label={item} key={item} />
                ))}
              </Radio.Group>
            </Group>
          </Group>
          <Group>
            <NumberInput
              disabled={form.getInputProps('type').value === PROPERTY_TYPES.Rental}
              label="Sale Price"
              min={0}
              step={10000}
              parser={(value) => value && value.replace(/\$\s?|(,*)/g, '')}
              formatter={(value) =>
                !Number.isNaN(parseFloat(value as string))
                  ? `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  : '$ '
              }
              {...form.getInputProps('pricing.salesPrice')}
            />
            <Group sx={{ alignItems: 'center' }}>
              <NumberInput
                disabled={form.getInputProps('type').value === PROPERTY_TYPES.Sale}
                label="Rental Price"
                min={0}
                step={100}
                parser={(value) => value && value.replace(/\$\s?|(,*)/g, '')}
                formatter={(value) =>
                  !Number.isNaN(parseFloat(value as string))
                    ? `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                    : '$ '
                }
                {...form.getInputProps('pricing.rentalPrice')}
              />
              <Select
                {...form.getInputProps('pricing.rentalDurationDivider')}
                disabled={form.values.type === PROPERTY_TYPES.Sale}
                label="per"
                data={RENTAL_DURATION_DIVIDERS.map((item) => ({
                  value: item,
                  label: RENTAL_DURATION_UNIT[item],
                }))}
                // defaultValue={RENTAL_DURATION_DIVIDERS[0]}
              />
            </Group>
          </Group>
          <Group>
            <NumberInput
              label="No. full beds"
              min={0}
              max={10}
              {...form.getInputProps('fullBedCount')}
            />
            <NumberInput
              label="No. full baths"
              min={0}
              max={10}
              {...form.getInputProps('fullBathCount')}
            />
            <NumberInput
              label="No. half beds"
              min={0}
              max={10}
              {...form.getInputProps('halfBedCount')}
            />
            <NumberInput
              label="No. half baths"
              min={0}
              max={10}
              {...form.getInputProps('halfBathCount')}
            />
          </Group>
          <Textarea
            label="Description"
            placeholder="Nice 4 bedrooms ranch, capitol beach"
            className={classes.fullWidth}
            autosize
            {...form.getInputProps('description')}
          />

          <Group>
            <AppendableFormListItems
              draggable
              onDragEnd={({ destination, source }) => {
                form.reorderListItem('contacts', {
                  from: source.index,
                  to: destination?.index || 0,
                });
              }}
              onAddField={() =>
                form.insertListItem('contacts', {
                  name: '',
                  email: '',
                  address: '',
                  phoneNumber: '',
                })
              }
              title="Contacts"
            >
              {form.values.contacts?.map((item, index) => (
                <Draggable key={index} index={index} draggableId={String(index)}>
                  {(provided) => (
                    <Group
                      ref={provided.innerRef}
                      p="xs"
                      {...provided.draggableProps}
                      align="center"
                    >
                      <Center {...provided.dragHandleProps}>
                        <Box>
                          <DragHandleDots2Icon />
                        </Box>
                      </Center>
                      <TextInput
                        withAsterisk
                        label="Name"
                        {...form.getInputProps(`contacts.${index}.name`)}
                      />
                      <TextInput
                        withAsterisk
                        label="Phone"
                        {...form.getInputProps(`contacts.${index}.phoneNumber`)}
                      />
                      <TextInput
                        label="Email"
                        {...form.getInputProps(`contacts.${index}.email`)}
                      />
                      <TextInput
                        label="Address"
                        {...form.getInputProps(`contacts.${index}.address`)}
                      />
                      <Box sx={{ display: 'flex', alignItems: 'end' }} pt={25}>
                        <ActionIcon
                          variant="outline"
                          color="red"
                          onClick={() => form.removeListItem('contacts', index)}
                        >
                          <MinusIcon />
                        </ActionIcon>
                      </Box>
                    </Group>
                  )}
                </Draggable>
              ))}
            </AppendableFormListItems>
          </Group>

          <Group>
            <YearSelection label="Built year" {...form.getInputProps('builtYear')} />
            <YearSelection
              label="Remodeled year"
              {...form.getInputProps('remodeledYear')}
            />
          </Group>
          <Group>
            <NumberInput
              label="Property Area (sqft)"
              min={0}
              {...form.getInputProps('propertyArea')}
            />
            <NumberInput
              label="Interior Area (sqft)"
              min={0}
              {...form.getInputProps('interiorArea')}
            />
          </Group>
          <Group>
            <Checkbox.Group
              {...form.getInputProps('flooringMaterials')}
              label="Flooring materials"
              defaultValue={form.getInputProps('flooringMaterials').value}
            >
              {Object.values(FLOORING_MATERIALS).map((item) => (
                <Checkbox value={item} label={item} key={item} />
              ))}
            </Checkbox.Group>
          </Group>
          <Group>
            <Checkbox.Group
              {...form.getInputProps('acTypes')}
              label="A/C Type"
              defaultValue={form.getInputProps('acTypes').value}
            >
              {Object.values(AC_TYPES).map((item) => (
                <Checkbox value={item} label={item} key={item} />
              ))}
            </Checkbox.Group>
          </Group>
          <Group>
            <AppendableFormListItems
              draggable={false}
              onAddField={() => form.insertListItem('highlights', '')}
            >
              <Checkbox.Group
                {...form.getInputProps('highlights')}
                p="xs"
                label="Highlights"
              >
                <Stack>
                  <Group>
                    {HIGHLIGHTS_BASE_CHOICES.map((item) => (
                      <Checkbox value={item} label={item} key={item}></Checkbox>
                    ))}
                  </Group>
                  <Group>
                    {form.values.highlights?.map((item, index) => (
                      <Group key={index}>
                        <TextInput
                          {...form.getInputProps(`highlights.${index}`)}
                          placeholder="Property highlights"
                        />
                        <ActionIcon
                          variant="outline"
                          color="red"
                          onClick={() => form.removeListItem('highlights', index)}
                        >
                          <MinusIcon />
                        </ActionIcon>
                      </Group>
                    ))}
                  </Group>
                </Stack>
              </Checkbox.Group>
            </AppendableFormListItems>
          </Group>
          <Group>
            <NumberInput
              label="No. of uncovered parking spots"
              min={0}
              {...form.getInputProps('parking.uncoveredSpots')}
            />
            <NumberInput
              label="No. of covered parking spots"
              min={0}
              {...form.getInputProps('parking.coveredSpots')}
            />
            <Group>
              <AppendableFormListItems
                draggable={false}
                onAddField={() => form.insertListItem('parking.features', '')}
              >
                <Checkbox.Group
                  {...form.getInputProps('parking.features')}
                  label="Parking features"
                  defaultValue={form.getInputProps('parking.features').value}
                  p="xs"
                >
                  {Object.values(PARKING_FEATURE_BASE_CHOICES).map((item) => (
                    <Checkbox value={item} label={item} key={item} />
                  ))}
                  <Group>
                    {form.values.parking?.features?.map((item, index) => (
                      <Group key={index}>
                        <TextInput
                          {...form.getInputProps(`parking.features.${index}`)}
                          placeholder="Parking feature"
                        />
                        <ActionIcon
                          variant="outline"
                          color="red"
                          onClick={() => form.removeListItem('parking.features', index)}
                        >
                          <MinusIcon />
                        </ActionIcon>
                      </Group>
                    ))}
                  </Group>
                </Checkbox.Group>
              </AppendableFormListItems>
            </Group>
          </Group>
          <Group className={classes.fullWidth}>
            <Checkbox.Group
              {...form.getInputProps('appliances')}
              label="Appliances"
              defaultValue={form.getInputProps('appliances').value}
            >
              {Object.values(APPLIANCES).map((item) => (
                <Checkbox value={item} label={item} key={item} />
              ))}
            </Checkbox.Group>
          </Group>
          <Group>
            <Box sx={{ width: '100%' }}>
              <Text weight={500} size="sm">
                Neighborhood details
              </Text>
            </Box>

            <AppendableFormListItems
              draggable={false}
              onAddField={() =>
                form.insertListItem('neighborhoodDetails.nearbySchools', '')
              }
              title="Nearby schools"
            >
              {form.values.neighborhoodDetails?.nearbySchools?.map((item, index) => (
                <Group p="xs" key={index}>
                  <TextInput
                    {...form.getInputProps(`neighborhoodDetails.nearbySchools.${index}`)}
                    placeholder="School of Rock"
                  />
                  <ActionIcon
                    variant="outline"
                    color="red"
                    onClick={() =>
                      form.removeListItem('neighborhoodDetails.nearbySchools', index)
                    }
                  >
                    <MinusIcon />
                  </ActionIcon>
                </Group>
              ))}
            </AppendableFormListItems>
            <AppendableFormListItems
              draggable={false}
              onAddField={() =>
                form.insertListItem('neighborhoodDetails.nearbyHospitals', '')
              }
              title="Nearby hospitals"
            >
              {form.values.neighborhoodDetails?.nearbyHospitals?.map((item, index) => (
                <Group p="xs" key={index}>
                  <TextInput
                    {...form.getInputProps(
                      `neighborhoodDetails.nearbyHospitals.${index}`,
                    )}
                    placeholder="Princeton General Hospital"
                  />
                  <ActionIcon
                    variant="outline"
                    color="red"
                    onClick={() =>
                      form.removeListItem('neighborhoodDetails.nearbyHospitals', index)
                    }
                  >
                    <MinusIcon />
                  </ActionIcon>
                </Group>
              ))}
            </AppendableFormListItems>
            <AppendableFormListItems
              draggable={false}
              onAddField={() =>
                form.insertListItem('neighborhoodDetails.nearbyRetails', '')
              }
              title="Nearby retail stores"
            >
              {form.values.neighborhoodDetails?.nearbyRetails?.map((item, index) => (
                <Group p="xs" key={index}>
                  <TextInput
                    {...form.getInputProps(`neighborhoodDetails.nearbyRetails.${index}`)}
                    placeholder="Walmart"
                  />
                  <ActionIcon
                    variant="outline"
                    color="red"
                    onClick={() =>
                      form.removeListItem('neighborhoodDetails.nearbyRetails', index)
                    }
                  >
                    <MinusIcon />
                  </ActionIcon>
                </Group>
              ))}
            </AppendableFormListItems>
          </Group>
          <Textarea
            label="HOA Information"
            autosize
            {...form.getInputProps('hoaInformation')}
            className={classes.fullWidth}
          />
          <Textarea
            label="Other"
            autosize
            {...form.getInputProps('otherInformation')}
            className={classes.fullWidth}
          />
          <Group
            p="sm"
            sx={{ position: 'sticky', bottom: 0, right: 0, width: '100%' }}
            position="center"
          >
            <Button type="submit">{labelSubmit}</Button>
            {Object.keys(form.errors).length ? (
              <Text color="red">Form contains error. Please check again!</Text>
            ) : (
              ''
            )}
            {property?._id && (
              <NextLink
                href={`${ROUTES.PROPERTY_ITEM.href.pathname}/${property._id}/upload`}
                target="_blank"
                passHref
              >
                <MyButton variant="outline">Upload images</MyButton>
              </NextLink>
            )}
            <MyButton onClick={toggle} variant="outline">
              Preview
            </MyButton>
          </Group>
        </Stack>
      </Center>
      <Modal
        opened={isOpen}
        onClose={close}
        centered
        fullScreen
        title={<MyPageTitle align="center">Preview</MyPageTitle>}
      >
        <PropertyPage property={form.values} propertyId={property?._id} />
      </Modal>
    </form>
  );
};
