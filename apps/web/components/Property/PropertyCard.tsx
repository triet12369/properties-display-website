import {
  Card,
  createStyles,
  Text,
  Stack,
  useMantineTheme,
  Skeleton,
  Box,
} from '@mantine/core';
import { NextLink } from '@mantine/next';
import Image from 'next/image';
import { FC } from 'react';

import { createImageURL, useSearchPropertyImages } from '../../api/getPropertyImages';
import placeholder from '../../assets/images/placeholder_house.jpg';
import { ROUTES } from '../../routes';
import { IPropertyDTO } from '../../types/dto/property';
import { RENTAL_DURATION_UNIT } from '../../types/units';

export type IPropertyCardProps = {
  data: IPropertyDTO;
};

const CARD_HEIGHT = 300;
const CARD_WIDTH = 350;
const IMG_FILL_RATIO = 0.7;

const useStyles = createStyles((theme) => ({
  card: {
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    '&:hover': {
      transform: 'scale(1.05)',
      cursor: 'pointer',
    },
    transition: `all 300ms ${theme.transitionTimingFunction}`,
  },
  imageWrapper: {
    height: `${IMG_FILL_RATIO * 100}%`,
    // width: '100%',
    // position: 'relative',
    // overflow: 'hidden',
  },
  image: {},
  pricesText: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
  },
}));

export const PropertyCardSkeleton: FC = () => {
  const { classes } = useStyles();
  const theme = useMantineTheme();

  return (
    <Card className={classes.card} shadow="md">
      <Card.Section>
        <Skeleton height={CARD_HEIGHT * IMG_FILL_RATIO} width={CARD_WIDTH} />
      </Card.Section>
      <Stack align="center" justify="center" p={theme.spacing.xs} spacing="xs">
        <Skeleton height={16} width={CARD_WIDTH / 2} />
        <Skeleton height={16} width={CARD_WIDTH / 2} />
        <Skeleton height={16} width={CARD_WIDTH / 2} />
      </Stack>
    </Card>
  );
};

export const PropertyCard: FC<IPropertyCardProps> = (props) => {
  const { data } = props;
  const { classes } = useStyles();
  const theme = useMantineTheme();
  const { data: images } = useSearchPropertyImages({ filter: { propertyId: data._id } });
  const previewImage = images?.length ? images[0] : undefined;
  const previewImageSrc = previewImage ? createImageURL(previewImage._id) : placeholder;
  const bedBathString = [
    data.fullBedCount && `${data.fullBedCount} bd`,
    data.fullBathCount && `${data.fullBathCount} ba`,
    data.propertyArea && `${data.propertyArea} sqft`,
  ]
    .filter((item) => !!item)
    .join(' | ');

  return (
    <NextLink href={`${ROUTES.PROPERTY_ITEM.href.pathname}/${data._id}`}>
      <Card className={classes.card} shadow="md" p={theme.spacing.xs}>
        <Card.Section className={classes.imageWrapper}>
          <Box
            sx={{
              width: previewImage?.width || '100%',
              height: previewImage?.height || '100%',
              maxWidth: '100%',
              maxHeight: '100%',
              position: 'relative',
            }}
          >
            <Image
              src={previewImageSrc}
              alt="house"
              className={classes.image}
              layout="fill"
              objectFit="cover"
            ></Image>
          </Box>
        </Card.Section>
        <Card.Section>
          <Stack
            align="center"
            justify="center"
            m="xs"
            spacing={4}
            sx={{ overflow: 'hidden' }}
          >
            <Text weight={'bold'} align="left">
              {bedBathString || '\u00A0'}
            </Text>
            <Text
              align="center"
              sx={{
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis',
                maxWidth: '100%',
              }}
            >
              {data.location
                ? `${data.location.address}, ${data.location.city}, ${data.location.state}, ${data.location.zip}`
                : ' '}
            </Text>
            <div className={classes.pricesText}>
              {data.pricing?.salesPrice ? (
                <Text weight={'bold'} color={theme.primaryColor}>
                  For Sale: {`$${data.pricing.salesPrice.toLocaleString('en-us')}`}
                </Text>
              ) : (
                ''
              )}

              {data.pricing?.rentalPrice ? (
                <Text weight={'bold'} color={theme.other.secondaryColor}>
                  For Rent: {`$${data.pricing.rentalPrice.toLocaleString('en-us')}`}/
                  {data.pricing.rentalDurationDivider
                    ? RENTAL_DURATION_UNIT[data.pricing.rentalDurationDivider]
                    : ''}
                </Text>
              ) : (
                ''
              )}
            </div>
          </Stack>
        </Card.Section>
      </Card>
    </NextLink>
  );
};
