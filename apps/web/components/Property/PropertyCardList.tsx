import { Carousel, CarouselProps } from '@mantine/carousel';
import { Center, createStyles, Group } from '@mantine/core';
import React from 'react';

import { IPropertyDTO } from '../../types/dto/property';

import { PropertyCardSkeleton, PropertyCard } from './PropertyCard';

const useStyles = createStyles((theme) => ({
  wrapper: {
    padding: theme.spacing.sm,
  },
}));

type IPropertyCardList = {
  data?: IPropertyDTO[];
} & (
  | {
      useCarousel?: true;
      CarouselProps?: CarouselProps;
    }
  | {
      useCarousel?: false;
    }
);

/**
 * Displays the list of properties
 */
export const PropertyCardList = (props: IPropertyCardList) => {
  const { data } = props;
  const { classes } = useStyles();
  if (!data?.length) return <></>;
  const Contents = data
    ? data.map((item) => <PropertyCard key={item._id} data={item}></PropertyCard>)
    : [<PropertyCardSkeleton />, <PropertyCardSkeleton />, <PropertyCardSkeleton />];
  if ('useCarousel' in props && props.useCarousel) {
    return (
      <Carousel
        slideSize="100%"
        slidesToScroll={1}
        slideGap="xs"
        align="center"
        withIndicators
        loop
        height="100%"
        py="lg"
        {...props.CarouselProps}
      >
        {Contents.map((Card) => (
          <Carousel.Slide>
            <Center p="md">{Card}</Center>
          </Carousel.Slide>
        ))}
      </Carousel>
    );
  }
  return (
    <Group position="center" spacing="sm" px={0} className={classes.wrapper}>
      {Contents}
    </Group>
  );
};
