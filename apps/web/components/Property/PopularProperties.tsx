import { createStyles, Text, Box } from '@mantine/core';
import React from 'react';

import { useSearchProperty } from '../../api/getProperties';

import { PropertyCardList } from './PropertyCardList';

const useStyles = createStyles((theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '100vw',
  },
  properties: {
    display: 'flex',
    flexDirection: 'row',
    maxWidth: '100vw',
  },
  mainText: {
    color: theme.white,
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    fontWeight: 900,
    lineHeight: 1.05,
    maxWidth: 500,
    fontSize: 48,
  },
}));

type IPopularProperties = {
  style?: React.CSSProperties;
};

/**
 * Displays the list of Popular Properties
 */
export const PopularProperties = (props: IPopularProperties) => {
  const { data } = useSearchProperty({
    filter: {
      isActive: true,
      isPopular: true,
    },
  });
  const { classes } = useStyles();
  return (
    <Box style={props?.style} className={classes.wrapper}>
      <Text className={classes.mainText}>Trending</Text>
      <PropertyCardList data={data} useCarousel />
    </Box>
  );
};
