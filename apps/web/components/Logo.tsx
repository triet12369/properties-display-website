import { createStyles } from '@mantine/core';
import Image from 'next/image';
import React, { forwardRef, ReactEventHandler, Ref } from 'react';

import headerLogo from '../assets/images/header_logo.png';

const useStyles = createStyles((theme) => ({
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'start',
    '&:hover': {
      cursor: 'pointer',
    },
  },
}));

type ILogoProps = {
  onClick?: ReactEventHandler<HTMLDivElement>;
};

function Logo(props: ILogoProps, ref: Ref<HTMLAnchorElement>) {
  const { onClick } = props;
  const { classes } = useStyles();
  return (
    <div onClick={onClick} className={classes.wrapper}>
      <Image src={headerLogo} alt={'logo'} layout="fill" objectFit="contain" />
    </div>
  );
}

export default forwardRef(Logo);
