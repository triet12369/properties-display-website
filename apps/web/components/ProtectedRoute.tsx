import { signOut, useSession } from 'next-auth/react';
import React, { FC, PropsWithChildren } from 'react';

import NoAccess from './NoAccess';

export const ProtectedRoute: FC<PropsWithChildren> = (props) => {
  const session = useSession();
  // check for expiry
  const jwt = session.data?.user.jwt;
  if (jwt && jwt.expire_at < new Date().getTime()) {
    signOut();
    return <NoAccess type="expired" />;
  }
  if (session.status === 'loading') return <NoAccess type="loading" />;
  if (session.status !== 'authenticated') return <NoAccess type="noaccess" />;
  return <>{props.children}</>;
};
