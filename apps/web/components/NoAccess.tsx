import {
  Box,
  Center,
  createStyles,
  keyframes,
  Title,
  useMantineTheme,
} from '@mantine/core';
import { ExclamationTriangleIcon } from '@modulz/radix-icons';
import React, { FC } from 'react';

import useGlobalStyles from '../styles/globalStyles';
import usePageContentStyles from '../styles/pageContentStyles';

const fadingPulse = keyframes`
  from {
    opacity: 1;
  }

  50% {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const useStyles = createStyles((theme) => ({
  darkenBackground: {
    opacity: '50%',
    backgroundColor: 'black',
  },
  wrapper: {
    position: 'relative',
    backgroundColor: 'black',
  },
  container: {
    position: 'absolute',
  },
  background: {
    opacity: '30%',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    position: 'absolute',
    animation: `${fadingPulse} 2s ease infinite`,
  },
}));
type NoAccessProps = {
  type: 'noaccess' | 'loading' | 'expired';
};

const NoAccess: FC<NoAccessProps> = (props) => {
  const { type } = props;
  const { classes: pageClasses } = usePageContentStyles();
  const { classes: globalClasses, cx } = useGlobalStyles();
  const { classes } = useStyles();
  const theme = useMantineTheme();
  const color =
    type === 'loading' ? theme.colors[theme.primaryColor][5] : theme.colors['orange'][6];

  let title;
  switch (type) {
    case 'noaccess':
      title = 'No access';
      break;
    case 'loading':
      title = 'Checking access...';
      break;
    case 'expired':
      title = 'Session expired. Please sign in again!';
      break;
  }
  return (
    <div
      className={cx(
        globalClasses.minContentHeight,
        globalClasses.centerContent,
        classes.wrapper,
      )}
    >
      <Box className={cx(pageClasses.background, classes.background)} />

      <Center className={classes.content}>
        <Box sx={{ width: '300px', height: '300px' }}>
          <ExclamationTriangleIcon color={color} width="100%" height="100%" />
        </Box>
        <Title color={color}>{title}</Title>
      </Center>
    </div>
  );
};

export default NoAccess;
