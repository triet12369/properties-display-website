import {
  createStyles,
  Divider,
  Footer,
  Text,
  Title,
  useMantineTheme,
} from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import React from 'react';

import { COMPANY_NAME, DEFAULT_CONTACT, FOOTER_HEIGHT, FOOTER_TEXT } from '../config';

const useStyles = createStyles((theme) => ({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    [theme.fn.smallerThan('sm')]: {
      flexDirection: 'column',
    },
  },
  footer: {
    zIndex: -1,
    backgroundColor: theme.colors[theme.primaryColor][9],
  },
  footerText: {
    fontFamily: 'Roboto',
    textAlign: 'center',
    fontWeight: 400,
    color: theme.colors.gray[3],
    width: '100%',
  },
  footerTextCompany: {
    fontWeight: 700,
    letterSpacing: '2px',
  },
}));

function MyFooter() {
  const { classes, cx } = useStyles();
  const theme = useMantineTheme();
  const isMobile = useMediaQuery(`(max-width: ${theme.breakpoints.sm}px)`, true, {
    getInitialValueInEffect: false,
  });
  return (
    <Footer
      withBorder={false}
      fixed
      height={FOOTER_HEIGHT}
      className={cx(classes.footer)}
    >
      <div className={classes.wrapper}>
        {!isMobile && <Text className={classes.footerText}>{FOOTER_TEXT}</Text>}
        <Text className={classes.footerText}>{DEFAULT_CONTACT.ADDRESS}</Text>
        <Title order={3} className={cx(classes.footerText, classes.footerTextCompany)}>
          © {COMPANY_NAME}
        </Title>
      </div>
    </Footer>
  );
}

export default MyFooter;
