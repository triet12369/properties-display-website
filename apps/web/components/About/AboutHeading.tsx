import { Title } from '@mantine/core';

import { COMPANY_NAME } from '../../config';

export default function AboutHeading() {
  return (
    <Title align="center" size={54}>
      {COMPANY_NAME}.
    </Title>
  );
}
