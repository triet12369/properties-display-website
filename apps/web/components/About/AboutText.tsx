import { createStyles, Text } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  formatParagraph: {
    color: theme.colors.dark[3],
    fontWeight: 400,
  },
}));

export default function AboutText() {
  const font = useStyles();
  return (
    <Text className={font.classes.formatParagraph}>
      Grand Opening in 2015, The Cornhusker Home Real Estate Company is the true for real
      estate, most innovative, creative and forward-thinking real estate company in
      Lincoln, Nebraska. At the core of our business philosophy is a commitment to
      extraordinary service, honesty, and clear communication. In brief, we are NOT your
      “average” real estate company. You’ll feel the difference the moment you walk into
      our offices, speak with us on the phone, or meet us in person. Our two experienced
      agents and investors, and full-time support staff are friendly, knowledgeable, and
      extremely hard-working. We combine the best in good old-fashioned customer service
      with the newest and brightest in technology and web-based marketing.
    </Text>
  );
}
