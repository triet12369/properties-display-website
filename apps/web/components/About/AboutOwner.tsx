import { createStyles, Text } from '@mantine/core';
import Image from 'next/image';

import PERSONNEL from '../../config/personnel';

const useStyles = createStyles((theme) => ({
  peopleWrapper: {
    // fontFamily: 'sans-serif',
    display: 'inline-flex',
  },
  roundAvatar: {
    width: '100px',
    height: '100px',
    position: 'relative',
    borderRadius: '50%',
    overflow: 'hidden',
    border: '0',
    backgroundColor: 'transparent',
  },
  personDetails: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    padding: 5,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  personName: {
    fontWeight: 500,
  },
  personPosition: {
    // fontWeight: 600,
    color: theme.colors.dark[3],
  },
}));

export default function AboutOwner() {
  const ownerFormat = useStyles();
  const personnelsComponents = PERSONNEL.map((person) => (
    <div key={person.name}>
      <div className={ownerFormat.classes.roundAvatar}>
        <Image quality="100" src={person.imageSrc} alt="Owner Picture" />
      </div>
      <Text className={ownerFormat.classes.personDetails}>
        <span className={ownerFormat.classes.personName}> {person.name} </span>
        <span className={ownerFormat.classes.personPosition}> {person.description} </span>
      </Text>
    </div>
  ));
  return <div className={ownerFormat.classes.peopleWrapper}>{personnelsComponents}</div>;
}
