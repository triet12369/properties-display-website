import { Header, Group, createStyles, Burger, Transition, Paper } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import Link from 'next/link';
import React from 'react';

import { HEADER_HEIGHT } from '../config';
import { ROUTES } from '../routes';

import Logo from './Logo';
import NavBar from './Navigation/NavBar';

const useStyles = createStyles((theme) => ({
  headerGroup: {
    display: 'flex',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  logoGroup: {
    width: '20%',
    height: '100%',
    [theme.fn.smallerThan('sm')]: {
      width: '100%',
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  navbarGroup: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'row-reverse',
    maxHeight: '100%',
    [theme.fn.smallerThan('sm')]: {
      display: 'none',
    },
  },
  burger: {
    position: 'absolute',
    left: theme.spacing.sm,
    [theme.fn.largerThan('sm')]: {
      display: 'none',
    },
  },
  dropdown: {
    position: 'absolute',
    top: HEADER_HEIGHT,
    left: 0,
    right: 0,
    zIndex: 0,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    borderTopWidth: 0,
    overflow: 'hidden',

    [theme.fn.largerThan('sm')]: {
      display: 'none',
    },
  },
}));

function MyHeader() {
  // const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  const [opened, { toggle, close }] = useDisclosure(false);
  const { classes } = useStyles();
  // const onClick = useCallback(() => {
  //   toggleColorScheme();
  // }, [toggleColorScheme]);

  return (
    <Header height={HEADER_HEIGHT} p="xs" withBorder>
      <Group className={classes.headerGroup} position="apart">
        <div className={classes.logoGroup}>
          <Link key={ROUTES.HOME.label} href={ROUTES.HOME.href} passHref>
            <Logo />
          </Link>
        </div>
        <div className={classes.navbarGroup}>
          <NavBar isDropdown={false} />
        </div>
        <Burger opened={opened} onClick={toggle} className={classes.burger} size="md" />
        <Transition transition="scale-y" duration={200} mounted={opened}>
          {(styles) => (
            <Paper
              className={classes.dropdown}
              withBorder
              style={styles}
              sx={{ zIndex: 10 }}
            >
              <NavBar isDropdown closeBurger={close} />
            </Paper>
          )}
        </Transition>
      </Group>
      {/* <Grid grow>
        <Grid.Col span={4}>
          <Logo />
        </Grid.Col>
        <Grid.Col span={8}>
          HIHI
        </Grid.Col>
      </Grid> */}
      {/* <Group grow position='apart'>
        <Group position="right">
          {process.env.APP_TITLE}
        </Group>
        <Group position="right">
          <ActionIcon
            onClick={onClick}
            size="xl"
            sx={(theme) => ({
              backgroundColor:
            theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
              color: theme.colorScheme === 'dark' ? theme.colors.yellow[4] : theme.colors.blue[6],
            })}
          >
            {colorScheme === 'dark' ? (
              <SunIcon width={20} height={20} />
            ) : (
              <MoonIcon width={20} height={20} />
            )}
          </ActionIcon>
        </Group>
      </Group> */}
    </Header>
  );
}

export default MyHeader;
