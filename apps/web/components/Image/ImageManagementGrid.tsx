import { Group, FileInput, ActionIcon, Center, Stack } from '@mantine/core';
import { ImageIcon, TrashIcon, UploadIcon } from '@modulz/radix-icons';
import React, { FC, useState } from 'react';
import { MyButton, MyTooltip } from 'ui';

import useGlobalStyles from '../../styles/globalStyles';
import { IPropertyImageDTO } from '../../types';

import { ImageCard } from './ImageCard';
import { ImageItem } from './ImageItem';

type ImageManagementGridProps = {
  imageIds: IPropertyImageDTO['_id'][];
  onUploadSubmit?: (files: File[]) => void;
  onDeleteSubmit?: (imageId: IPropertyImageDTO['_id']) => void;
};

export const ImageManagementGrid: FC<ImageManagementGridProps> = (props) => {
  const { imageIds, onUploadSubmit, onDeleteSubmit } = props;
  const { classes: globalClasses, cx } = useGlobalStyles();
  const [files, setFiles] = useState<File[]>([]);

  const handleUpload = () => {
    onUploadSubmit && onUploadSubmit(files);
  };

  const onUploadInputChange = (value: File[]) => {
    setFiles(value);
  };
  return (
    <Group position="center">
      {imageIds.map((imageId) => (
        <ImageCard
          key={String(imageId)}
          ImageComponent={<ImageItem key={imageId.toString()} imageId={imageId} />}
          FooterComponent={
            <MyTooltip label="Delete">
              <ActionIcon
                mx="sm"
                size="md"
                className={cx(globalClasses.actionIcon, globalClasses.hoverHighlight)}
                onClick={() => onDeleteSubmit && onDeleteSubmit(imageId)}
              >
                <TrashIcon color="red" />
              </ActionIcon>
            </MyTooltip>
          }
        />
      ))}
      <ImageCard
        ImageComponent={
          <Center sx={{ width: '100%', height: '100%' }} p="xs">
            <Stack sx={{ width: '100%', height: '100%' }} align="center" justify="center">
              <FileInput
                sx={{ width: '100%', overflow: 'hidden' }}
                onChange={onUploadInputChange}
                label="Upload images"
                multiple
                accept="image/png,image/jpeg"
                icon={<ImageIcon color="green" />}
              />
              <MyTooltip label="Upload">
                <MyButton variant="outline" color="green" onClick={handleUpload}>
                  <UploadIcon />
                </MyButton>
              </MyTooltip>
            </Stack>
          </Center>
        }
        FooterComponent={<></>}
      />
    </Group>
  );
};
