import { Card, Group } from '@mantine/core';
import { FC, ReactNode } from 'react';

type ImageCardProps = {
  ImageComponent: ReactNode;
  FooterComponent: ReactNode;
};
export const ImageCard: FC<ImageCardProps> = (props) => {
  const { ImageComponent, FooterComponent } = props;
  return (
    <Card
      shadow="sm"
      p="xs"
      radius="md"
      withBorder
      sx={{ width: '350px', height: '300px' }}
    >
      <Card.Section sx={{ height: '90%', overflow: 'hidden' }}>
        {ImageComponent}
      </Card.Section>
      <Group position="center" mt="md" mb="xs" sx={{ height: '10%' }}>
        {FooterComponent}
      </Group>
    </Card>
  );
};
