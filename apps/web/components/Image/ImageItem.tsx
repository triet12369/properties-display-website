import { Image, ImageProps } from '@mantine/core';
import React, { FC } from 'react';

import { createImageURL } from '../../api/getPropertyImages';
import { IPropertyImageDTO } from '../../types';

type ImageItemProps = {
  imageId: IPropertyImageDTO['_id'];
} & ImageProps;
export const ImageItem: FC<ImageItemProps> = (props) => {
  const { imageId, ...rest } = props;
  const imgSrc = createImageURL(imageId);
  return (
    // <Box sx={{ width: '100%', height: '100%' }}>
    <Image
      src={imgSrc}
      sx={{ width: '100%', height: '100%' }}
      fit="cover"
      alt="property image"
      withPlaceholder
      {...rest}
    />
    // </Box>
  );
};
