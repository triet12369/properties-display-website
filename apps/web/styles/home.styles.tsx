import { createStyles } from '@mantine/core';

import { MIN_CONTENT_HEIGHT } from './pageContentStyles';

const useStyles = createStyles((theme) => {
  return {
    contentWrapper: {
      zIndex: 2,
      paddingTop: theme.spacing.xl * 2,
      paddingBottom: theme.spacing.xl * 2,
      paddingLeft: theme.spacing.xl * 2,
      paddingRight: theme.spacing.xl * 2,

      [theme.fn.smallerThan('md')]: {
        marginRight: 0,
        padding: theme.spacing.xs,
      },
    },
    description: {
      color: theme.white,
      opacity: 0.75,
      maxWidth: 500,

      [theme.fn.smallerThan('md')]: {
        maxWidth: '100%',
      },
    },
    welcomeContent: {
      display: 'flex',
      minHeight: MIN_CONTENT_HEIGHT,
      justifyContent: 'center',
      flexDirection: 'column',
      position: 'relative',
      [theme.fn.smallerThan('sm')]: {
        minHeight: '80vh',
      },
    },
    downIcon: {
      position: 'absolute',
      bottom: 0,
      [theme.fn.smallerThan('sm')]: {
        position: 'relative',
      },
    },
  };
});

export default useStyles;
