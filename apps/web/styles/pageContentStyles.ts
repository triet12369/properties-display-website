import { keyframes } from '@emotion/react';
import { createStyles } from '@mantine/core';

import homeBackground from '../assets/images/home_background.jpg';
import { HEADER_HEIGHT } from '../config';

export const MIN_CONTENT_HEIGHT = `calc(100vh - ${HEADER_HEIGHT}px)`;

const animation = keyframes`
  from {
    opacity: 0;
  }

  50% {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const usePageContentStyles = createStyles((theme) => {
  const darkenedBackgroundColor = theme.fn.darken(
    theme.colors[theme.primaryColor][9],
    0.8,
  );
  return {
    wrapper: {
      minHeight: MIN_CONTENT_HEIGHT,
      minWidth: '100%',
      backgroundColor: theme.colors.gray[1],
      boxShadow:
        '0px 3px 5px -1px rgba(0,0,0,0.2),0px 5px 8px 0px rgba(0,0,0,0.14),0px 1px 14px 0px rgba(0,0,0,0.12)',
    },
    background: {
      position: 'relative',
      display: 'flex',
      height: '100%',
      flexDirection: 'column',
      width: '100%',
      justifyContent: 'center',
      backgroundImage: `url(${homeBackground.src})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      zIndex: 0,
    },
    linearGradient: {
      position: 'absolute',
      top: 0,
      width: '100%',
      height: '100%',
      background: `linear-gradient(250deg, rgba(255, 255, 255, 0.1) 0%, ${darkenedBackgroundColor} 70%)`,
      [theme.fn.smallerThan('md')]: {
        padding: theme.spacing.xs,
        background: `linear-gradient(180deg, ${darkenedBackgroundColor} 40%, rgba(255, 255, 255, 0.1) 80%, ${darkenedBackgroundColor} 100%)`,
      },
      animation: `${animation} 1000ms ease`,
    },
  };
});

export default usePageContentStyles;
