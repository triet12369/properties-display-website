import { createStyles } from '@mantine/core';

import { MIN_CONTENT_HEIGHT } from './pageContentStyles';

const useGlobalStyles = createStyles((theme) => ({
  centerContentSmallerThanMd: {
    [theme.fn.smallerThan('md')]: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
    },
  },
  rounded: {
    borderRadius: '5px',
  },
  hoverHighlight: {
    color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

    '&:hover': {
      backgroundColor:
        theme.colorScheme === 'dark'
          ? theme.colors.dark[8]
          : theme.colors[theme.primaryColor][0],
      transition: 'background-color 500ms ease',
    },
  },
  minContentHeight: {
    height: MIN_CONTENT_HEIGHT,
  },
  fillParent: {
    width: '100%',
    height: '100%',
  },
  centerContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionIcon: {
    '& > svg': {
      width: '100%',
      height: '100%',
      maxHeight: '25px',
    },
  },
}));

export default useGlobalStyles;
