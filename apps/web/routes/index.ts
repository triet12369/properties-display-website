/**
 * Define Routes configuration
 */

import { PROPERTY_TYPES } from '../../server/constants';
import { RouteItem } from '../types';

const cs = (param: RouteItem) => param;

export const ROUTES = {
  HOME: cs({
    label: 'Home',
    href: {
      pathname: '/',
    },
  }),
  PROPERTY_LIST_BUY: cs({
    label: 'Buy',
    href: {
      pathname: '/property',
      query: {
        type: PROPERTY_TYPES.Sale,
      },
    },
  }),
  PROPERTY_LIST_RENT: cs({
    label: 'Rent',
    href: {
      pathname: '/property',
      query: {
        type: PROPERTY_TYPES.Rental,
      },
    },
  }),
  PROPERTY_ITEM: cs({
    label: 'Property Detail',
    href: {
      pathname: '/property/item',
    },
  }),
  PROPERTY_MANAGEMENT: cs({
    label: 'Property management',
    href: {
      pathname: '/property/management',
    },
  }),
  PROPERTY_CREATE: cs({
    label: 'Property create',
    href: {
      pathname: '/property/item/create',
    },
  }),
  ABOUT: cs({
    label: 'About',
    href: {
      pathname: '/about',
    },
  }),
  AUTH_ERROR: {
    label: 'Authentication error',
    href: {
      pathname: '/auth/error',
    },
  },
} as const;
