import { useQuery } from 'react-query';

import { PROPERTY } from '../../server/db/resources';
import { axios } from '../lib/axios';
import { ExtractFnReturnType, QueryConfig } from '../lib/react-query';
import { IPropertyFilter } from '../types';
import { IPropertyDTO } from '../types/dto/property';

export const getManyProperties = (filter: IPropertyFilter): Promise<IPropertyDTO[]> => {
  return axios.post(`/api/${PROPERTY}/search`, filter);
};

export const getOneProperty = ({
  propertyId,
}: {
  propertyId: string;
}): Promise<IPropertyDTO> => {
  return axios.get(`/api/${PROPERTY}/${propertyId}`);
};

type QueryOnePropertyFnType = typeof getOneProperty;

type QuerySearchPropertyFnType = typeof getManyProperties;

type IUseProperty = {
  propertyId: string;
  config?: QueryConfig<QueryOnePropertyFnType>;
};

export const useProperty = ({ propertyId, config }: IUseProperty) => {
  return useQuery<ExtractFnReturnType<QueryOnePropertyFnType>>({
    ...config,
    queryKey: [PROPERTY, propertyId],
    queryFn: () => getOneProperty({ propertyId }),
  });
};

type IUseSearchProperty = {
  filter: IPropertyFilter;
  config?: QueryConfig<QuerySearchPropertyFnType>;
};

export const useSearchProperty = ({ filter, config }: IUseSearchProperty) => {
  return useQuery<ExtractFnReturnType<QuerySearchPropertyFnType>>({
    ...config,
    queryKey: [PROPERTY, filter],
    queryFn: () => getManyProperties(filter),
  });
};
