import { showNotification } from '@mantine/notifications';
import { useRouter } from 'next/router';
import { useMutation, useQueryClient } from 'react-query';

import { JWT } from '../../server/types';
import { axios } from '../lib/axios';
import { MutationConfig } from '../lib/react-query';

import { IPropertyImageDTO } from './../types/dto/propertyImage';
import { PROPERTY_IMAGE } from './resources';

export const deletePropertyImage = (args: {
  imageId: IPropertyImageDTO['_id'];
  jwt?: JWT;
}): Promise<unknown> => {
  if (!args.jwt) {
    showNotification({
      message: 'Session expired, please login again!',
      color: 'red',
    });
    return Promise.reject('Session expired.');
  }
  return axios.delete(`/api/${PROPERTY_IMAGE}/${args.imageId}`, {
    headers: {
      Authorization: `${args.jwt.type} ${args.jwt.access_token}`,
    },
  });
};

type UseDeleteImageOptions = {
  config?: MutationConfig<typeof deletePropertyImage>;
};

export const useDeleteImage = ({ config }: UseDeleteImageOptions) => {
  // const { addNotification } = useNotificationStore();
  const router = useRouter();
  const queryClient = useQueryClient();
  const mutation = useMutation({
    onError: () => {
      showNotification({
        message: 'Image delete error.',
        color: 'red',
      });
    },
    onSuccess: () => {
      showNotification({
        message: 'Image deleted successfully.',
      });
      router.replace(router.asPath);
      queryClient.invalidateQueries([PROPERTY_IMAGE]);
    },
    ...config,
    mutationFn: deletePropertyImage,
  });

  return mutation;
};
