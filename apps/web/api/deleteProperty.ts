import { showNotification } from '@mantine/notifications';
import { useMutation, useQueryClient } from 'react-query';

import { JWT } from '../../server/types';
import { axios } from '../lib/axios';
import { MutationConfig } from '../lib/react-query';

import { PROPERTY } from './resources';

export const deleteProperty = (args: {
  propertyId: string;
  jwt?: JWT;
}): Promise<unknown> => {
  if (!args.jwt) {
    showNotification({
      message: 'Session expired, please login again!',
      color: 'red',
    });
    return Promise.reject('Session expired.');
  }
  return axios.delete(`/api/property/${args.propertyId}`, {
    headers: {
      Authorization: `${args.jwt.type} ${args.jwt.access_token}`,
    },
  });
};

type UseCreateJobOptions = {
  jwt?: JWT;
  config?: MutationConfig<typeof deleteProperty>;
};

export const useDeleteProperty = ({ config }: UseCreateJobOptions) => {
  // const { addNotification } = useNotificationStore();
  const queryClient = useQueryClient();
  const mutation = useMutation({
    onError: () => {
      showNotification({
        message: 'Property delete error.',
        color: 'red',
      });
    },
    onSuccess: (context) => {
      showNotification({
        message: 'Property deleted successfully.',
      });
      queryClient.invalidateQueries([PROPERTY]);
    },
    ...config,
    mutationFn: deleteProperty,
  });

  return mutation;
};
