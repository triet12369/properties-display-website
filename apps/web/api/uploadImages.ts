import { showNotification } from '@mantine/notifications';
import { useRouter } from 'next/router';
import { useMutation } from 'react-query';

import { JWT } from '../../server/types';
import { axios } from '../lib/axios';
import { MutationConfig } from '../lib/react-query';

import { PROPERTY_IMAGE } from './resources';

export const uploadImages = (args: {
  propertyId: string;
  files: File[];
  jwt?: JWT;
}): Promise<unknown> => {
  if (!args.jwt) {
    showNotification({
      message: 'Session expired, please login again!',
      color: 'red',
    });
    return Promise.reject('Session expired.');
  }
  const { files, jwt, propertyId } = args;
  const form = new FormData();
  files.forEach((file) => form.append(PROPERTY_IMAGE, file));

  return axios.post(`/api/${PROPERTY_IMAGE}/upload/${propertyId}`, form, {
    headers: {
      Authorization: `${jwt.type} ${jwt.access_token}`,
      'Content-Type': 'multipart/form-data',
    },
  });
};

type UseUploadImagesProps = {
  jwt?: JWT;
  config?: MutationConfig<typeof uploadImages>;
};

export const useUploadImages = ({ config }: UseUploadImagesProps) => {
  const router = useRouter();
  const mutation = useMutation({
    onError: () => {
      showNotification({
        message: 'Image upload error.',
        color: 'red',
      });
    },
    onSuccess: () => {
      showNotification({
        message: 'Images updated successfully.',
      });
      router.reload();
    },
    ...config,
    mutationFn: uploadImages,
  });

  return mutation;
};
