import { showNotification } from '@mantine/notifications';
import { useMutation } from 'react-query';

import { JWT } from '../../server/types';
import { axios } from '../lib/axios';
import { MutationConfig } from '../lib/react-query';
import { IPropertyForm } from '../types/dto';

export const updateProperty = (args: {
  propertyId: string;
  body: IPropertyForm;
  jwt?: JWT;
}): Promise<unknown> => {
  if (!args.jwt) {
    showNotification({
      message: 'Session expired, please login again!',
      color: 'red',
    });
    return Promise.reject('Session expired.');
  }
  return axios.patch(`/api/property/${args.propertyId}`, args.body, {
    headers: {
      Authorization: `${args.jwt.type} ${args.jwt.access_token}`,
    },
  });
};

type UseCreateJobOptions = {
  config?: MutationConfig<typeof updateProperty>;
};

export const useUpdateProperty = ({ config }: UseCreateJobOptions) => {
  // const { addNotification } = useNotificationStore();
  const mutation = useMutation({
    onError: () => {
      showNotification({
        message: 'Property update error.',
        color: 'red',
      });
    },
    onSuccess: () => {
      showNotification({
        message: 'Property updated successfully.',
      });
    },
    ...config,
    mutationFn: updateProperty,
  });

  return mutation;
};
