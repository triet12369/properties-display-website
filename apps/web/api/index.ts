import httpProxy from 'http-proxy';
import { NextApiRequest, NextApiResponse } from 'next';

import { API_URL } from '../config';
import { axios } from '../lib/axios';
import { IJWT, ILoginForm } from '../types';

export const login = (loginForm: ILoginForm): Promise<IJWT> => {
  return axios.post('/api/login', loginForm);
};
const proxy = httpProxy.createProxyServer();

const proxiedRequest = (req: NextApiRequest, res: NextApiResponse) => {
  proxy.web(req, res, { target: API_URL, changeOrigin: true });
};

export default proxiedRequest;
