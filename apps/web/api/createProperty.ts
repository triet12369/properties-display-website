import { showNotification } from '@mantine/notifications';
import { useMutation } from 'react-query';

import { JWT } from '../../server/types';
import { axios } from '../lib/axios';
import { MutationConfig } from '../lib/react-query';
import { IPropertyForm } from '../types/dto';

export const createProperty = (args: {
  body: IPropertyForm;
  jwt?: JWT;
}): Promise<unknown> => {
  if (!args.jwt) {
    showNotification({
      message: 'Session expired, please login again!',
      color: 'red',
    });
    return Promise.reject('Session expired.');
  }
  return axios.post('/api/property', args.body, {
    headers: {
      Authorization: `${args.jwt.type} ${args.jwt.access_token}`,
    },
  });
};

type UseCreateJobOptions = {
  jwt?: JWT;
  config?: MutationConfig<typeof createProperty>;
};

export const useCreateProperty = ({ config }: UseCreateJobOptions) => {
  // const { addNotification } = useNotificationStore();
  const mutation = useMutation({
    onError: () => {
      showNotification({
        message: 'Property creation error.',
        color: 'red',
      });
    },
    onSuccess: () => {
      showNotification({
        message: 'Property created successfully.',
      });
    },
    ...config,
    mutationFn: createProperty,
  });

  return mutation;
};
