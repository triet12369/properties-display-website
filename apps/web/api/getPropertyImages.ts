import { useQuery } from 'react-query';

import { axios } from '../lib/axios';
import { ExtractFnReturnType, QueryConfig } from '../lib/react-query';
import { IPropertyImageFilter } from '../types';
import { IPropertyImageDTO } from '../types/dto/propertyImage';

import { PROPERTY_IMAGE } from './resources';

export const createImageURL = (imageId: IPropertyImageDTO['_id']) =>
  `/api/${PROPERTY_IMAGE}/data/${imageId}`;

export const getManyPropertyImages = (
  filter: IPropertyImageFilter,
): Promise<IPropertyImageDTO[]> => {
  return axios.post(`/api/${PROPERTY_IMAGE}/search`, filter);
};

export const getOneImage = ({
  imageId,
}: {
  imageId: IPropertyImageDTO['_id'];
}): Promise<ArrayBuffer> => {
  return axios.get(`/api/${PROPERTY_IMAGE}/data/${imageId}`);
};

type QueryOnePropertyImageFnType = typeof getOneImage;

type QuerySearchPropertyFnType = typeof getManyPropertyImages;

type IUsePropertyImage = {
  imageId: IPropertyImageDTO['_id'];
  config?: QueryConfig<QueryOnePropertyImageFnType>;
};

export const usePropertyImage = ({ imageId, config }: IUsePropertyImage) => {
  return useQuery<ExtractFnReturnType<QueryOnePropertyImageFnType>>({
    ...config,
    queryKey: [PROPERTY_IMAGE, imageId],
    queryFn: () => getOneImage({ imageId }),
  });
};

type IUseSearchPropertyImages = {
  filter: IPropertyImageFilter;
  config?: QueryConfig<QuerySearchPropertyFnType>;
};

export const useSearchPropertyImages = ({ filter, config }: IUseSearchPropertyImages) => {
  return useQuery<ExtractFnReturnType<QuerySearchPropertyFnType>>({
    ...config,
    queryKey: [PROPERTY_IMAGE, filter],
    queryFn: () => getManyPropertyImages(filter),
    staleTime: 30000,
  });
};
