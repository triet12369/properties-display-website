const withTM = require('next-transpile-modules')(['ui', 'properties-display-server']);

module.exports = withTM({
  reactStrictMode: false, // react-dnd does not play nice with StrictMode
  swcMinify: true,
  // expose these environment variables to clients
  env: {
    APP_TITLE: process.env.APP_TITLE,
    BASE_URL: process.env.BASE_URL,
  },
  images: {
    domains: [process.env.BASE_URL, process.env.API_URL, 'localhost', '192.168.0.161'],
  },
  rewrites: async () => [
    {
      source: '/api/auth/:slug*',
      destination: '/api/auth/:slug*',
    },
    {
      source: '/api/:slug*',
      destination: `${process.env.API_URL}/:slug*`,
    },
  ],
});
