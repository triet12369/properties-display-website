FROM node:16.18.0-buster AS base
# RUN apk update
WORKDIR /app

# FROM base AS pruner
# # RUN apk add --no-cache libc6-compat
# RUN yarn global add turbo@1.5.5
# COPY . .
# RUN turbo prune --scope=${SCOPE} --docker

FROM base AS deps
COPY . .
RUN yarn install --frozen-lockfile

FROM base AS builder
COPY --from=deps /app/node_modules ./node_modules
COPY . .
RUN yarn build
# RUN find . -name node_modules | xargs rm -rf

FROM base AS runner
ENV NODE_ENV production
# Uncomment the following line in case you want to disable telemetry during runtime.
# ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY --from=builder --chown=nextjs:nodejs /app/. .

# RUN chown -R nextjs:nodejs ./node_modules

USER nextjs
RUN yarn global add turbo@1.5.5

EXPOSE 3000

CMD ["yarn", "start"]
