#!/bin/bash

echo "./apps/server/.env ./apps/web/.env" | xargs -n 1 cp .env
docker build . -f ./Dockerfile -t properties-display-service:latest